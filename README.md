FORZA4
======

Forza4 può essere compilato con 2 interfacce diverse:

 * _ncurses_ (**DEFAULT**) (che richiede la libreria ncurses);
 * _old_ (compatibile con qualunque sistema).

Per compilare con l'interfaccia di default basta il comando

    make all

Per compilare, invece, con l'interfaccia old:

    make interfaccia_old

Per aprire Forza4, eseguire il file

    main

Su Ubuntu le dipendenze richieste sono:

    * make
    * gcc
    * libncurses5-dev (per l'interfaccia di default)

Forza4 è compatibile con MacOS nella versione interfaccia old.
