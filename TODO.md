TODO
=====

* controllare i commenti e che ci siano i prototipi
* (**FATTO**) controllare che a tutte le funzioni siano passati i pvoid delle istanze delle classi
* sistemare le questioni pubblico/privato per le struct delle classi
* controllare i nomi delle funzioni: nomeclasse_nomefunzione
* (**FATTO**) controllare che il tipo pvoid sia preso sempre come p\_nomeclasse_
* (**FATTO**) controllare che il tipo delle struct relative alle classi sia \_nomeclasse
* (**FATTO**) cambiare INT_MIN in INT_MIN+1 come errore di euristica
* rimuovere tutte le funzioni non esistenti dai file .h