==============
## MINIMAX: ##
==============

Requires: nothing


Tipi dato:
===========
Privati:

Pubblici:


Variabili:
===========

Puntatori a funzione:
- euristica
    (situazione)                    -→ punteggio
    
- inserisci pedina
    (situazione, giocatore, mossa)  -→ void
    
- rimuovi pedina
    (situazione, giocatore, mossa)  -→ void


Funzioni:
==========

- minimax
    (situazione, profondità)        -→ (punteggio, mossa)
    
- inizializza funzioni
    (euristica, inserisci pedina)   -→ void




===================
## FILE MANAGER: ##
===================

Requires: nothing


Funzioni:
==========

- salva struct
    (nomefile, pstruct, size)           -→ (ok/errore)

- carica struct
    (nomefile, pstruct, size)           -→ (ok/errore)



===================
## FORZA4 - CORE ##
===================

Requires: Minimax, File manager


Tipi dato:
===========
Privati:
- modello (array che contiene tutti i coefficienti)

Pubblici:
- scacchiera (struct che contiene tutte e 4 le scacchiere, con tutte
        le informazioni che vogliamo portarci, tipo altezza colonne)

- impostazioni (struct con tutte le impostazioni dentro, così si
        salvano subito)
            N.B. difficoltà (profondità minimax), #iterazioni allenamento
                prima di salvare, dim. sacchiera


Variabili:
===========
Puntatori a funzione:

Dati:
- impostazioni
- modello

Funzioni:
==========

- allena
    (#iterazioni, modello iniziale,
        peso modello iniziale,
        print percentuale allenamento)      -→ modello
            N.B. print percentuale allenamento è puntatore a
                funzione del tipo (percentuale) -→ (ok/errore)

- euristica
    (scacchiera)                                        -→ punteggio

- inserisci pedina
    (scacchiera, giocatore, mossa)                      -→ void

- rimuovi pedina
    (scacchiera, giocatore, mossa)                      -→ void
        
- carica allenamento da file
    ()                                                  -→ modello
    
- salva allenamento su file
    (modello)                                           -→ (ok/errore)

        



============
## FORZA4 ##
============

Requires: Forza4-core, File manager


Tipi dato:
===========
Privati:

Pubblici:


Variabili:
===========

Puntatori a funzione:
- print scacchiera
    (scacchiera)                            -→ (ok/errore)

- scanf mossa
    (scacchiera, giocatore)                 -→ mossa

- print percentuale allenamento
    (percentuale)                           -→ (ok/errore)
    
- menu impostazioni
    (array (nomi valori, vecchi valori))    -→ (valore, nuovo valore)

Dati:

Funzioni:
==========

- gioca con pc
    ()                                                  -→ (vinto/perso)

- gioca con umano
    ()                                                  -→ (vinto/perso)

- collega interfaccia
    (print scacchiera, scanf mossa,
        menu impostazioni,
        print percentuale allenamento)                  -→ void

- carica impostazioni da file
    ()                                                  -→ impostazioni
    
- salva impostazioni su file
    (impostazioni)                                      -→ (ok/errore)

- impostazioni
    ()                                                  -→ void
        N.B. utilizza la funzione menu impostazioni dell'interfaccia
            per cambiare la variabile impostazioni, poi salva tutto
            su file
        
        



==================
## INTERFACCIA: ##
==================

Requires: Forza4, Forza4-core

Tipi dato:
===========

Variabili:
===========

Puntatori a funzione:

Funzioni:
==========

- menu
    ()                                      -→ void
        N.B. allena, gioca con pc, gioca con umano, impostazioni, esci
        N.B. continua finché non esci
    
- menu impostazioni
    (puntatore a impostazioni)              -→ void

- print scacchiera
    (scacchiera)                            -→ (ok/errore)

- scanf mossa
    (scacchiera, giocatore)                 -→ mossa

- print percentuale allenamento
    (percentuale)                           -→ (ok/errore)
