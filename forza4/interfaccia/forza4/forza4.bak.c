void forza4_salva_allenamento(pvoid pforza4_, pvoid _pcoefficienti)
 {
  my_int j;
  pmy_int pcoefficienti;
  pvoid plista_campi, pimpostazioni;
  FILE* pf;

  pcoefficienti = (pmy_int)_pcoefficienti;

  // salvo allenamento su file txt
  printf("Salvo allenamento su file txt . . . ");
  pf = fopen("coefficienti_vincitore_prova.txt", "w");
  for(j=0;j<NUMERO_FEATURES;j++)
   {
    fprintf(pf, "%d\n", pcoefficienti[j]);
   }
  fclose(pf);
  printf("Fatto!\n");


  // salvo allenamento su file bin
  printf("Salvo allenamento su file bin . . . ");
  plista_campi = lista_campi_init();
  lista_campi_add(plista_campi, 0, sizeof(my_int)*NUMERO_FEATURES, 0);
  pimpostazioni = impostazioni_init(plista_campi, "prova_della_prova.bin");
  impostazioni_save(pimpostazioni, pcoefficienti);
  printf("Fatto!\n");

  lista_campi_free(plista_campi);
  impostazioni_free(pimpostazioni);
 }
