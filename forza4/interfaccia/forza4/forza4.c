#include "../../../include/debug/debug.h"
#include "../../../include/minimax/minimax.h"
#include "../../../include/impostazioni/impostazioni.h"
#include "../../../include/impostazioni/lista_campi/lista_campi.h"
#include "stato_partita/stato_partita.h"
#include "forza4.h"
#include "forza4.hidden.h"
#include "../../../include/my_defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <string.h>

#define NUMERO_FEATURES 1024

p_forza4_impostazioni forza4_impostazioni_init()
{
    p_forza4_impostazioni pimpostazioni = (p_forza4_impostazioni) malloc(sizeof(_forza4_impostazioni));

    if (pimpostazioni == NULL) return NULL;

    pimpostazioni->coefficienti_filename = (pchar) malloc(COEFFICIENTI_FILENAME_MAXLEN);
    if (pimpostazioni->coefficienti_filename == NULL)
    {
        free(pimpostazioni);
        return NULL;
    }

    return pimpostazioni;
}

void forza4_impostazioni_free(p_forza4_impostazioni pimpostazioni)
{
    free(((p_forza4_impostazioni)pimpostazioni)->coefficienti_filename);
    free(pimpostazioni);
}

void forza4_impostazioni_default_fill(pvoid pforza4_)
{
    p_forza4_impostazioni pimpostazioni = ((p_forza4)pforza4_)->pimpostazioni;

    strcpy(pimpostazioni->coefficienti_filename, DEFAULT_COEFFICIENTI_PATH);
    pimpostazioni->h=6;
    pimpostazioni->w=7;
    pimpostazioni->minimax_depth=DEFAULT_DIFFICULTY;
}

void forza4_impostazioni_apply(pvoid pforza4_)
{
    p_forza4 pforza4 = (p_forza4) pforza4_;

    forza4_set_size(pforza4_, pforza4->pimpostazioni->h, pforza4->pimpostazioni->w);
}

my_int forza4_impostazioni_load(pvoid pforza4_)
{
    p_forza4 pforza4 = (p_forza4) pforza4_;
    my_int res;

    forza4_impostazioni_default_fill(pforza4_);
    //printf("%dx%d\n", pforza4->pimpostazioni->w, pforza4->pimpostazioni->h);
    res=impostazioni_load(pforza4->pimpostazioni_impostazioni_, pforza4->pimpostazioni);
    //printf("%dx%d\n", pforza4->pimpostazioni->w, pforza4->pimpostazioni->h);
    
    forza4_impostazioni_apply(pforza4_);

    return res;
}

my_int forza4_impostazioni_save(pvoid pforza4_)
{
    p_forza4 pforza4 = (p_forza4) pforza4_;

    return impostazioni_save(pforza4->pimpostazioni_impostazioni_, pforza4->pimpostazioni);
}

pvoid forza4_init()
{
    p_forza4 pforza4 = (p_forza4) malloc(sizeof(_forza4));
    if (pforza4 == NULL) return NULL;

    pforza4->pstato_partita_ = NULL;
    pforza4->print_stato_partita = NULL;
    pforza4->scanf_mossa = NULL;

    pforza4->pminimax_ = NULL;
    pforza4->pstato_partita_ = NULL;

    pforza4->pimpostazioni = forza4_impostazioni_init();
    if (pforza4->pimpostazioni == NULL)
    {
        free(pforza4);
        return NULL;
    }
    
    pvoid plista_campi_impostazioni = lista_campi_init();
    if (plista_campi_impostazioni == NULL)
    {
        forza4_impostazioni_free(pforza4->pimpostazioni);
        free(pforza4);
        return NULL;
    }

    lista_campi_add_(plista_campi_impostazioni, 2, 
            LISTA_CAMPI_GET_RELATIVE_STRUCT_POS(pforza4->pimpostazioni, w), sizeof(my_int), 0); // scacchiera width
    lista_campi_add_(plista_campi_impostazioni, 3,
            LISTA_CAMPI_GET_RELATIVE_STRUCT_POS(pforza4->pimpostazioni, h), sizeof(my_int), 0); // scacchiera height
    lista_campi_add_(plista_campi_impostazioni, 4,
            LISTA_CAMPI_GET_RELATIVE_STRUCT_POS(pforza4->pimpostazioni, minimax_depth), sizeof(my_int), 0); // minimax depth
    lista_campi_add_(plista_campi_impostazioni, 1,
           LISTA_CAMPI_GET_RELATIVE_STRUCT_POS(pforza4->pimpostazioni, coefficienti_filename) , COEFFICIENTI_FILENAME_MAXLEN, 1); // coefficienti_filename

    pforza4->pimpostazioni_impostazioni_ = impostazioni_init(plista_campi_impostazioni, IMPOSTAZIONI_DEFAULT_PATH);
    if (pforza4->pimpostazioni_impostazioni_ == NULL)
    {
        forza4_impostazioni_free(pforza4->pimpostazioni);
        lista_campi_free(plista_campi_impostazioni);
        free(pforza4);
        return NULL;
    }

    forza4_impostazioni_load(pforza4);
    
    return (pvoid)pforza4;
}

void forza4_free(pvoid pforza4_)
{
    p_forza4 pforza4 = (p_forza4) pforza4_;
    
    forza4_impostazioni_free(pforza4->pimpostazioni);
    impostazioni_free(pforza4->pimpostazioni_impostazioni_);
    
    if (pforza4->pminimax_!=NULL)
    {
        minimax_free(pforza4->pminimax_);
    }

    if (pforza4->pstato_partita_ != NULL)
    {
        stato_partita_free(pforza4->pstato_partita_);
    }

    free(pforza4);
}

my_int forza4_set_size(pvoid pforza4_, my_int rows, my_int columns)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;

    if (rows<4||columns<4)
        return 0;

    pforza4->pimpostazioni->h=rows;
    pforza4->pimpostazioni->w=columns;

    // riinizializza stato partita
    if (pforza4->pstato_partita_!=NULL)
    {
        stato_partita_free(pforza4->pstato_partita_);
    }
    
    pforza4->pstato_partita_ = stato_partita_init(rows, columns);
    if (pforza4->pstato_partita_ == NULL)
    {
        return 0;
    }
    stato_partita_fill(pforza4->pstato_partita_, 0);
    
    stato_partita_pcoefficienti_set_filename(pforza4->pstato_partita_, pforza4->pimpostazioni->coefficienti_filename);

    // riinizializza minimax
    if (pforza4->pminimax_!=NULL)
    {
        minimax_free(pforza4->pminimax_);
    }

    pforza4->pminimax_ = minimax_init(stato_partita_inserisci_pedina, stato_partita_rimuovi_pedina, stato_partita_check_vittoria, columns, EURISTICA_PUNTEGGIO_VITTORIA, EURISTICA_PUNTEGGIO_PERDITA);
    if (pforza4->pminimax_ == NULL)
    {
        stato_partita_free(pforza4->pstato_partita_);
        return 0;
    }

    minimax_set_euristica(pforza4->pminimax_, stato_partita_euristica_globale);
    minimax_set_aggiorna_euristica(pforza4->pminimax_, stato_partita_aggiorna_euristica, AGGIORNA_EURISTICA_ERRORE);

    return 1;
}

void forza4_set_print(pvoid pforza4_, p_print_stato_partita_t print_stato_partita_)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;

    pforza4->print_stato_partita = print_stato_partita_;
}

void forza4_set_scanf_mossa(pvoid pforza4_, p_scanf_mossa_t scanf_mossa_)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;
    
    pforza4->scanf_mossa = scanf_mossa_;
}

void forza4_set_show_hide_message(pvoid pforza4_, p_print_show_message_t show_message_, p_print_hide_message_t hide_message_)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;
    
    pforza4->show_message = show_message_;
    pforza4->hide_message = hide_message_;
}

void forza4_set_wait(pvoid pforza4_, p_wait_t wait_)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;

    pforza4->wait = wait_;
}

void forza4_set_print_hide_big(pvoid pforza4_, p_print_big_t print_big_, p_print_hide_big_t hide_big_)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;

    pforza4->print_big = print_big_;
    pforza4->hide_big = hide_big_;
}

void forza4_set_print_hide_percentage(pvoid pforza4_, p_print_percentage_t print_percentage, p_print_percentage_hide_t hide_percentage)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;

    pforza4->print_percentage = print_percentage;
    pforza4->hide_percentage = hide_percentage;
}

void forza4_set_difficulty(pvoid pforza4_, my_int difficulty)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;
    if ((difficulty>=MIN_DIFFICULTY)&&(difficulty<=MAX_DIFFICULTY))
        pforza4->pimpostazioni->minimax_depth = difficulty;
}

my_int forza4_get_cell(pvoid pforza4_, my_int row, my_int column)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;
    return stato_partita_get(pforza4->pstato_partita_, row, column);
}

my_int forza4_get_difficulty(pvoid pforza4_)
{
    p_forza4 pforza4 = (p_forza4)pforza4_;
    return pforza4->pimpostazioni->minimax_depth;
}

my_int forza4_gioca_vs_umano(pvoid pforza4_, pvoid pinterfaccia_)
 {
  p_forza4 pforza4 = (p_forza4)pforza4_;
  pvoid pstato_ = pforza4->pstato_partita_;
  int i, mossa, giocatore, vittoria;

  char testo[40];

  i=giocatore=vittoria=mossa=0;
  stato_partita_azzera(pstato_);
  
  while(vittoria==0)
   {
    giocatore = (i%2)+1;

    pforza4->print_stato_partita(pinterfaccia_, pforza4_);
    //interfaccia_old_print_scacchiera(pstato);
    
    //printf("Mossa giocatore %d: ", giocatore);
    //scanf("%d", &mossa);
    mossa = pforza4->scanf_mossa(pinterfaccia_, pforza4_, giocatore);

    if (mossa>0)
    {
        if (stato_partita_inserisci_pedina(pstato_, giocatore, mossa-1)) i++;
        vittoria = stato_partita_check_vittoria(pstato_);
    }
    else if (mossa<0)
    {
        goto end_match_with_man;
    }
   }

  //interfaccia_old_print_scacchiera(pstato);
  pforza4->print_stato_partita(pinterfaccia_, pforza4_);
  //getch();

  //pforza4->print_result(pinterfaccia_, vittoria);
  if(vittoria == -1)
  {
      pforza4->show_message(pinterfaccia_, "La partita e' finita in parità!\n");
  }
  else
  {
      sprintf(testo, "Giocatore %d ha vinto!\n", vittoria);
      pforza4->show_message(pinterfaccia_, testo);
  }
  //printf("La partita e' durata %d mosse.\n", i);

  pforza4->wait();

  end_match_with_man:
  pforza4->hide_message(pinterfaccia_);

  return vittoria;
 }

my_int forza4_gioca_vs_pc(pvoid pforza4_, pvoid pinterfaccia_)
 {
  p_forza4 pforza4 = (p_forza4)pforza4_;
  int i, mossa, giocatore, vittoria, score;
  pvoid pstato_;
  pvoid pminimax_;
  pmy_int pcoefficienti;
  char text[40];

  i=giocatore=vittoria=mossa=0;
  pstato_ = pforza4->pstato_partita_;
  pminimax_ = pforza4->pminimax_;

  pcoefficienti = stato_partita_pcoefficienti_init();

  for(i=0; i<NUMERO_FEATURES; i++) pcoefficienti[i] = 0;

  if (!stato_partita_pcoefficienti_load(pstato_, pcoefficienti))
  {
    pforza4->show_message(pinterfaccia_, "Non e' stato possibile caricare il file!");
    pforza4->wait();
    pforza4->hide_message(pinterfaccia_);
    
    return 0;
  }


  stato_partita_azzera(pstato_);

  i=0;
  while(vittoria==0)
   {
    giocatore = (i%2)+1;

    pforza4->print_stato_partita(pinterfaccia_, pforza4_);
    
    if(giocatore==1)
     {
      mossa=pforza4->scanf_mossa(pinterfaccia_, pforza4_, giocatore);
      if (mossa<0)
      {
          goto end_match_with_pc;
      }
     }
    else
     {
      mossa = 1+minimax_start(pminimax_, pstato_, pcoefficienti, pforza4->pimpostazioni->minimax_depth, &score, 2);
     }

    if (mossa>0)
    {
        if (stato_partita_inserisci_pedina(pstato_, giocatore, mossa-1)) i++;
        vittoria = stato_partita_check_vittoria(pstato_);
    }
   }

  pforza4->print_stato_partita(pinterfaccia_, pforza4_);

  if (vittoria == -1)
  {
      pforza4->show_message(pinterfaccia_, "La partita e' finita in parità!\n");
  }
  else
  {
      sprintf(text, "Giocatore %d ha vinto!\n", vittoria);
      pforza4->show_message(pinterfaccia_, text);
  }
  
  sprintf(text, "La partita e' durata %d mosse.\n", i);
  
  pforza4->wait();
  
  pforza4->hide_message(pinterfaccia_);

  pforza4->show_message(pinterfaccia_, text);

  pforza4->wait();

  end_match_with_pc:
  stato_partita_pcoefficienti_free(pcoefficienti);
  pforza4->hide_message(pinterfaccia_);

  return vittoria;
 }

inline int forza4_allena_pc_vs_pc(p_forza4 pforza4, pvoid pinterfaccia_, pvoid pcoefficienti1, pvoid pcoefficienti2)
{
    my_int vittoria, numero_mossa, giocatore, mossa;
    pvoid   pstato_ = pforza4->pstato_partita_,
            pminimax_ = pforza4->pminimax_;
    my_int colonne = stato_partita_getW(pstato_);
    
    vittoria=numero_mossa=0;
    giocatore = (rand()&1)?1:2;
    stato_partita_azzera(pstato_);

    while(vittoria==0)
    {
        //giocatore = (numero_mossa%2)+1; //a+b=2; 2a+b=1 => a=-1
        giocatore = 3 - giocatore;

        if(numero_mossa==0 || numero_mossa==1)
        {
            mossa = rand()%(colonne);
        }
        else
        {
            if(giocatore==1)
                mossa = minimax_start(pminimax_, pstato_, pcoefficienti1, pforza4->pimpostazioni->minimax_depth, NULL, 1);
            else
                mossa = minimax_start(pminimax_, pstato_, pcoefficienti2, pforza4->pimpostazioni->minimax_depth, NULL, 2);
        }
        
        if (mossa!=MINIMAX_NO_MOVE_FOUND)
        {
            if (stato_partita_inserisci_pedina(pstato_, giocatore, mossa))
            {
                numero_mossa++;
                vittoria = stato_partita_check_vittoria(pstato_);
            }
            else
            {
                vittoria = -1;
            }
        }
    } // fine while, cioè fine partita

    //interfaccia_print_scacchiera(NULL, pstato); // stampa la scacchiera alla fine della partita
    return vittoria;
}

inline void forza4_allena_internal(p_forza4 pforza4, pvoid pinterfaccia_, my_int numero_partite_per_nupla, my_int numero_nuple_da_generare, pvoid _pcoefficienti_vincenti)
 {
  pvoid pstato_ = pforza4->pstato_partita_;
  my_int numero_partita, numero_nupla, mossa, vittoria, vittorie_giocatore1, vittorie_giocatore2, vincitore_allenamento;
  pmy_int pcoefficienti1, pcoefficienti2, pcoefficienti_da_modificare, paus, pcoefficienti_vincenti;

  vittoria=mossa=vincitore_allenamento=0;
  pcoefficienti_vincenti = (pmy_int)_pcoefficienti_vincenti;

  char txt_out[400];

  pcoefficienti1 = stato_partita_pcoefficienti_init();
  pcoefficienti2 = stato_partita_pcoefficienti_init();

  srand(time(NULL));
  
  // genero 2 n-uple casuali
  stato_partita_pcoefficienti_randfill(pstato_, pcoefficienti1);
  stato_partita_pcoefficienti_randfill(pstato_, pcoefficienti2);

  pforza4->print_big(pinterfaccia_, "Inizio allenamento!");

  sprintf(txt_out, "Totale partite per n-upla: %d\nTotale n-uple da generare: %d\n ", numero_partite_per_nupla, numero_nuple_da_generare);
  pforza4->print_big(pinterfaccia_, txt_out);
  
  for(numero_nupla=0; numero_nupla<numero_nuple_da_generare; numero_nupla++)
   {
    pforza4->print_percentage(pinterfaccia_, 100*numero_nupla/numero_nuple_da_generare);
    sprintf(txt_out, "Round %d.\n", numero_nupla+1);
    pforza4->print_big(pinterfaccia_, txt_out);
    //printf("Round %d.\n", j+1);

    #if _DEBUG_ALLENAMENTO
    pf = fopen("coefficienti.txt", "a");
    fprintf(pf, "Inizio ciclo di partite %d ----------------------\n", numero_nupla);
    fprintf(pf, "Primi coefficienti di giocatore 1:\n");
    for(i=0;i<10;i++)
     {
      fprintf(pf, "%d\n", pcoefficienti1[i]);
     }
    fprintf(pf, "Primi coefficienti di giocatore 2:\n");
    for(i=0;i<10;i++)
     {
      fprintf(pf, "%d\n", pcoefficienti2[i]);
     }
    fprintf(pf, "\n");
    fclose(pf);
    #endif

    vittorie_giocatore1 = vittorie_giocatore2 = 0;
    for(numero_partita=0; numero_partita<numero_partite_per_nupla; numero_partita++)
     {
        vittoria = forza4_allena_pc_vs_pc(pforza4, pinterfaccia_, pcoefficienti1, pcoefficienti2);
        
        #if STAMPA_ALLENAMENTO
        if(vittoria == -1)
        {
            printf("La partita è finita in parità!\n");
            
        }
        #endif

        if (vittoria != -1)
        {
            #if STAMPA_ALLENAMENTO
            printf("Giocatore %d ha vinto!\n", vittoria);
            #endif
            if(vittoria == 1) vittorie_giocatore1++; else vittorie_giocatore2++;
        }

        #if STAMPA_ALLENAMENTO
        printf("La partita è durata %d mosse.\n", i);
        #endif
     } // fine for delle partite

    vincitore_allenamento = (vittorie_giocatore1>=vittorie_giocatore2)?1:2;

    sprintf(txt_out, "Giocatore 1 ha vinto: %d partite\nGiocatore 2 ha vinto: %d partite\nVince giocatore %d!\n", vittorie_giocatore1, vittorie_giocatore2, vincitore_allenamento);
    pforza4->print_big(pinterfaccia_, txt_out);
    
    /*printf("Giocatore 1 ha vinto: %d partite\n", vittorie_giocatore1);
    printf("Giocatore 2 ha vinto: %d partite\n", vittorie_giocatore2);
    printf("Vince giocatore %d!\n", vincitore_allenamento);
    printf("\n");*/


    // dal secondo ciclo di partite in poi, modifico i coefficienti del perdente
    pcoefficienti_da_modificare = (vittorie_giocatore1>=vittorie_giocatore2)?
                                        pcoefficienti2:
                                        pcoefficienti1;
      
    stato_partita_pcoefficienti_randfill(pstato_, pcoefficienti_da_modificare);
   } // fine for delle n-uple
  
  pforza4->hide_percentage(pinterfaccia_);

  //printf("Alla fine dell'allenamento ha vinto giocatore %d.\n", vincitore_allenamento);
  if(vincitore_allenamento==1) paus = pcoefficienti1; else paus = pcoefficienti2; // paus punta all'n-upla vincente

  stato_partita_pcoefficienti_copy(pstato_, paus, pcoefficienti_vincenti);

  stato_partita_pcoefficienti_free(pcoefficienti1);
  stato_partita_pcoefficienti_free(pcoefficienti2);
 }

void forza4_allena(pvoid pforza4_, pvoid pinterfaccia_, my_int numero_cicli, my_int numero_nuple_da_generare, my_int numero_partite_per_nupla)
 {
  p_forza4 pforza4 = (p_forza4) pforza4_;
  my_int i;
  pvoid pcoefficienti_vincenti, pcoefficienti_best_sofar;

  my_int were_there_coefficients;

  char out_txt[100];

  pcoefficienti_vincenti = stato_partita_pcoefficienti_init();
  pcoefficienti_best_sofar = stato_partita_pcoefficienti_init();

  were_there_coefficients = stato_partita_pcoefficienti_load(pforza4->pstato_partita_, pcoefficienti_best_sofar);

  for(i=0; i<numero_cicli; i++)
   {
    sprintf(out_txt, "Inizio ciclo di allenamento %d. -----------------------------------------------\n\n", i+1);
    pforza4->print_big(pinterfaccia_, out_txt);
    forza4_allena_internal(pforza4, pinterfaccia_, numero_partite_per_nupla, numero_nuple_da_generare, (pvoid)pcoefficienti_vincenti);

    if (were_there_coefficients==0)
    {
        stato_partita_pcoefficienti_copy(pforza4->pstato_partita_, pcoefficienti_vincenti, pcoefficienti_best_sofar);
        were_there_coefficients=1;
    }
    else
    {
        if (2==forza4_allena_pc_vs_pc(pforza4, pinterfaccia_, pcoefficienti_best_sofar, pcoefficienti_vincenti))
        {
            stato_partita_pcoefficienti_copy(pforza4->pstato_partita_, pcoefficienti_vincenti, pcoefficienti_best_sofar);
        }
    }

    if (!stato_partita_pcoefficienti_save(pforza4->pstato_partita_, pcoefficienti_best_sofar))
    {
        pforza4->show_message(pinterfaccia_, "Non e' stato possibile salvare il file");
        pforza4->wait();
        pforza4->hide_message(pinterfaccia_);
        goto stop_allenamento;
    }

    sprintf(out_txt, "Fine ciclo di allenamento %d. ------------------------------------------------\n \n", i+1);
    pforza4->print_big(pinterfaccia_, out_txt);
   }

  stop_allenamento:
  stato_partita_pcoefficienti_free(pcoefficienti_best_sofar);
  stato_partita_pcoefficienti_free(pcoefficienti_vincenti);
  
  pforza4->wait();
  pforza4->hide_message(pinterfaccia_);
  pforza4->hide_big(pinterfaccia_);
 }

my_int forza4_getH(pvoid pforza4_)
{
    p_forza4 pforza4 = (p_forza4) pforza4_;

    return stato_partita_getH(pforza4->pstato_partita_);
}

my_int forza4_getW(pvoid pforza4_)
{
    p_forza4 pforza4 = (p_forza4) pforza4_;

    return stato_partita_getW(pforza4->pstato_partita_);
}