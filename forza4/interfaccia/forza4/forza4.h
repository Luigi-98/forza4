#ifndef _FORZA4_H
#define _FORZA4_H

#include "../../../include/my_defines.h"
#include "tipi_puntatori_funzioni.h"

#define IMPOSTAZIONI_DEFAULT_PATH "settings.bin"
#define DEFAULT_COEFFICIENTI_PATH "coefficienti.bin"
#define DEFAULT_DIFFICULTY  3

#define SUGGESTED_N_CICLI_ALLENAMENTO   2
#define SUGGESTED_N_NUPLE_PER_CICLO 100
#define SUGGESTED_N_PARTITE_PER_NUPLA_ALLENAMENTO 10

#define MIN_DIFFICULTY 0
#define MAX_DIFFICULTY INT_MAX

#define COEFFICIENTI_FILENAME_MAXLEN 20

/*
 * Come utilizzare la classe forza4.
 * =================================
 * 
 * Step obbligatori:
 * 1. costruire l'oggetto con forza4_init()
 * 2. impostare la dimensione della scacchiera con forza4_set_size(...)
 * 3. impostare tutte le funzioni di interfaccia contenute nel file tipi_puntatori_funzioni.h
 * 4. alla fine distruggere l'oggetto con forza4_free(...)
 * 
 * Le **impostazioni** saranno caricate automaticamente alla costruzione dell'oggetto,
 * ma possono essere ricaricate, modificate e salvate durante l'esecuzione del programma.
 * 
 * Per **giocare contro un giocatore virtuale** bisogna prima fare allenare il computer
 * utilizzando la funzione forza4_allena(...).
 * */



/*
 * Costruttore e distruttore della classe forza4
 * 
 * returns      NULL se andato male
 *              oggetto se andato bene
 * */
pvoid forza4_init();
void forza4_free(pvoid pforza4_);

/*
 * Carica, salva, reimposta ai valori predefiniti e applica le impostazioni
 * 
 * impostazioni_load non richiede impostazioni_apply
 * 
 * returns      vero se caricato/salvato con successo
 *              falso altrimenti
 * */
my_int forza4_impostazioni_load(pvoid pforza4_);
my_int forza4_impostazioni_save(pvoid pforza4_);
void forza4_impostazioni_default_fill(pvoid pforza4_);
void forza4_impostazioni_apply(pvoid pforza4_);

/*
 * Permette di estrarre valori dalla scacchiera:
 *  * il valore della casella di coordinate (row, column)
 *  * altezza della scacchiera
 *  * larghezza della scacchiera
 * */
my_int forza4_get_cell(pvoid pforza4_, my_int row, my_int column);
my_int forza4_getH(pvoid pforza4_);
my_int forza4_getW(pvoid pforza4_);

/*
 * Permette di estrarre la difficoltà di gioco attualmente impostata
 * 
 * Compresa tra MIN_DIFFICULTY e MAX_DIFFICULTY
 * 
 * Più alta è la difficoltà, maggiore è il tempo di calcolo della mossa successiva
 * */
my_int forza4_get_difficulty(pvoid pforza4_);

/*
 * Imposta/reimposta la dimensione della scacchiera.
 * 
 * Va chiamata prima di ogni funzione di gioco o allenamento.
 * 
 * returns      vero se impostata con successo
 *              falso altrimenti
 * */
my_int forza4_set_size(pvoid pforza4_, my_int rows, my_int columns);

/*
 * Imposta le funzioni di interfaccia (descritte in tipi_puntatori_funzioni.h).
 * 
 * Vanno impostate tutte prima di ogni operazione.
 * */
void forza4_set_scanf_mossa(pvoid pforza4_, p_scanf_mossa_t scanf_mossa_);
void forza4_set_print(pvoid pforza4_, p_print_stato_partita_t print_stato_partita_);
void forza4_set_show_hide_message(pvoid pforza4_, p_print_show_message_t show_message_, p_print_hide_message_t hide_message_);
void forza4_set_wait(pvoid pforza4_, p_wait_t wait_);
void forza4_set_print_hide_big(pvoid pforza4_, p_print_big_t print_big_, p_print_hide_big_t hide_big_);
void forza4_set_print_hide_percentage(pvoid pforza4_, p_print_percentage_t print_percentage, p_print_percentage_hide_t hide_percentage);

/*
 * Imposta la difficoltà di gioco.
 * 
 * Compresa tra MIN_DIFFICULTY e MAX_DIFFICULTY
 * 
 * Più alta è la difficoltà, maggiore è il tempo di calcolo della mossa successiva
 * */
void forza4_set_difficulty(pvoid pforza4_, my_int difficulty);

/*
 * Gioca contro un altro giocatore.
 * 
 * returns      1 se vince il primo giocatore,
 *              2 se vince il secondo giocatore,
 *              -1 in caso di pareggio
 * */
my_int forza4_gioca_vs_umano(pvoid pforza4_, pvoid pinterfaccia_);

/*
 * Gioca contro un giocatore virtuale.
 * 
 * returns      1 se vince il giocatore umano,
 *              2 se vince il computer,
 *              -1 in caso di pareggio
 * */
my_int forza4_gioca_vs_pc(pvoid pforza4_, pvoid pinterfaccia_);

/*
 * Avvia l'algoritmo di learning con i metaparametri scelti.
 * 
 * numero_cicli                 numero di cicli di learning da completare
 * numero_nuple_da_generare     numero di set di parametri di apprendimento da generare
 * numero_partite_per_nupla     numero di partite da giocare per testare la bontà di un set di parametri
 * 
 * Mostra un log e una percentuale durante l'allenamento.
 * */
void forza4_allena(pvoid pforza4_, pvoid pinterfaccia_, my_int numero_cicli, my_int numero_nuple_da_generare, my_int numero_partite_per_nupla);

#endif
