#ifndef _FORZA4_HIDDEN
#define _FORZA4_HIDDEN

#include "forza4.h"

typedef pint modello;
typedef modello* pmodello;

typedef struct __forza4_impostazioni
{
    my_int w;
    my_int h;
    my_int minimax_depth;
    pchar coefficienti_filename;
} _forza4_impostazioni;

typedef _forza4_impostazioni* p_forza4_impostazioni;

typedef struct __forza4
{
    p_print_stato_partita_t print_stato_partita;
    p_scanf_mossa_t scanf_mossa;
    p_print_show_message_t show_message;
    p_print_hide_message_t hide_message;
    p_print_percentage_t print_percentage;
    p_print_percentage_hide_t hide_percentage;
    p_wait_t wait;
    p_print_big_t print_big;
    p_print_hide_big_t hide_big;
    pvoid pstato_partita_;
    pvoid pminimax_;
    pvoid pimpostazioni_impostazioni_;
    pvoid pcoefficienti_;

    p_forza4_impostazioni pimpostazioni;
} _forza4;

typedef _forza4* p_forza4;

#endif
