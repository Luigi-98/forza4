pstato_partita malloc_stato_partita(int righe, int colonne)
{
    if ((righe<=0)||(colonne<=0))
    {
        return NULL;
    }
    
    pstato_partita pstato = (pstato) malloc(sizeof(stato_partita));
    
    if (pstato==NULL) return pstato;
    
    pstato->pscacchiera_verticale = (pint) malloc(colonne*((righe+ncoppieperint-1)/ncoppieperint)); // = colonne * ceiling(righe/ncoppieperint)
    if (pstato->pscacchiera_verticale==NULL)
    {
        free(pstato);
        return NULL;
    }
    pstato->pscacchiera_orizzontale = (pint) malloc(righe*((colonne+ncoppieperint-1)/ncoppieperint));
    if (pstato->pscacchiera_orizzontale==NULL)
    {
        free(pstato->pscacchiera_verticale);
        free(pstato);
        return NULL;
    }
    pstato->pscacchiera_diagonale1 = (pint) malloc((colonne+righe-1)*((righe+ncoppieperint-1)/ncoppieperint));
    if (pstato->pscacchiera_diagonale1==NULL)
    {
        free(pstato->pscacchiera_verticale);
        free(pstato->pscacchiera_orizzontale);
        free(pstato);
        return NULL;
    }
    pstato->pscacchiera_diagonale2 = (pint) malloc((colonne+righe-1)*((righe+ncoppieperint-1)/ncoppieperint));
    if (pstato->pscacchiera_diagonale2==NULL)
    {
        free(pstato->pscacchiera_verticale);
        free(pstato->pscacchiera_orizzontale);
        free(pstato->pscacchiera_diagonale1);
        free(pstato);
        return NULL;
    }
    
    return pstato;
}

void free(pstato_partita pstato)
{
    free(pstato->pscacchiera_verticale);
    free(pstato->pscacchiera_orizzontale);
    free(pstato->pscacchiera_diagonale1);
    free(pstato->pscacchiera_diagonale2);
    free(pstato);
}
