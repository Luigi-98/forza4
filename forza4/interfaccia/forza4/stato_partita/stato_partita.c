#include "stato_partita.hidden.h"
#include "stato_partita.h"
#include "../../../../include/my_defines.h"
#include "../../../../include/debug/debug.h"
#include "../../../../include/minimax/minimax.h"
#include "../../../../include/impostazioni/impostazioni.h"
#include "../../../../include/impostazioni/lista_campi/lista_campi.h"
#include "../../../../include/utils/math_utils.h"
#include "../../../../include/utils/dump_utils.h"

#include <limits.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>


#define NUMERO_FEATURES 1024

#define _DEBUG_MALLOC 0
#define _DEBUG_FEATURES 0
#define _DEBUG_FEATURES_VERT_ORIZZ 0
#define _DEBUG_FEATURES_DIAG 0
#define _DEBUG_ALLENAMENTO 0
#define STAMPA_ALLENAMENTO 0
#define SCRIVI_ALLENAMENTO_SU_FILE 1

#define NCOPPIEPERINT ((my_int)((CHAR_BIT>>1)*sizeof(my_int)))

#define _NCOPPIEPERINT_IS_POWEROFTWO _SIZEOF_INT_IS_POWEROFTWO&&_CHAR_BITS_IS_POWEROFTWO

#if _NCOPPIEPERINT_IS_POWEROFTWO
 #define DIVIDEBYNCOPPIEPERINT(n) ((my_uint)(n)>>logncoppieperint)
 #define MODULEBYNCOPPIEPERINT(n) ((n)&(NCOPPIEPERINT-1))
#else
 #define DIVIDEBYNCOPPIEPERINT(n) ((n)/NCOPPIEPERINT)
 #define MODULEBYNCOPPIEPERINT(n) ((n)%NCOPPIEPERINT)
#endif

my_int logncoppieperint;

pvoid stato_partita_init(my_uint righe, my_uint colonne)
{
    if ((righe<4)||(colonne<4))
    {
        return NULL;
    }

    logncoppieperint=log_2(NCOPPIEPERINT);
    
    // (x+y-1)/y = ceiling(x/y)
    my_int
        sof_col=DIVIDEBYNCOPPIEPERINT(righe+NCOPPIEPERINT-1),
        sof_row=DIVIDEBYNCOPPIEPERINT(colonne+NCOPPIEPERINT-1);
    my_int
        sof_scacc_vert=colonne*sof_col,
        sof_scacc_diag=(colonne+righe-1)*sof_col,
        sof_scacc_oriz=righe*sof_row;
    
    // alloco lo spazio che mi serve sia per la struct che per il contenuto
    p_stato_partita pstato_partita = (p_stato_partita) malloc(sizeof(_stato_partita)
                            + (sof_scacc_vert // scacchiera verticale
                            + sof_scacc_oriz // scacchiera orizzontale
                            + 2*sof_scacc_diag // scacchiera diagonale 1 e 2
                            + colonne) * sizeof(my_int) // altezza colonne
                            );
    
    #if _DEBUG_MALLOC
    printf("Righe x colonne = %dx%d\n\n", righe, colonne);
    printf("NCOPPIEPERINT: %d\n\n", NCOPPIEPERINT);
    printf("Sofs (in ints):\n\
            row:        %d\n\
            col:        %d\n\
            scacc_vert: %d\n\
            scacc_diag: %d\n\
            scacc_oriz: %d\n\n", sof_row, sof_col, sof_scacc_vert, sof_scacc_diag, sof_scacc_oriz);
    printf("Sizes (in chars):\n\
            sof my_int: %d\n\
            sof pmy_int: %d\n\n", sizeof(my_int), sizeof(pmy_int));
    printf("Allocated sizes (in chars):\n\
            sof struct: %d\n\
            sof scacchiere: %d\n\
            sof array col: %d\n", sizeof(_stato_partita), (sof_scacc_vert + sof_scacc_oriz + 2*sof_scacc_diag)*sizeof(my_int), (colonne) * sizeof(my_int));
    printf("Total allocated size (in chars): %d\n", sizeof(_stato_partita)
                            + (sof_scacc_vert // scacchiera verticale
                            + sof_scacc_oriz // scacchiera orizzontale
                            + 2*sof_scacc_diag // scacchiera diagonale 1 e 2
                            + colonne) * sizeof(my_int));
    #endif

    if (pstato_partita==NULL) return NULL;
    
    // ripartisco lo spazio a chi di dovere
    pstato_partita->pscacchiera_verticale = (pmy_int) ((pvoid)pstato_partita
                                        + sizeof(_stato_partita));
    pstato_partita->pscacchiera_orizzontale = pstato_partita->pscacchiera_verticale
                                        + sof_scacc_vert;
    pstato_partita->pscacchiera_diagonale1 = pstato_partita->pscacchiera_orizzontale
                                        + sof_scacc_oriz;
    pstato_partita->pscacchiera_diagonale2 = pstato_partita->pscacchiera_diagonale1
                                        + sof_scacc_diag;
    pstato_partita->paltezza_colonne = pstato_partita->pscacchiera_diagonale2
                                        + sof_scacc_diag;
    
    pstato_partita->righe=righe;
    pstato_partita->colonne=colonne;
    
    pstato_partita->sof_row=sof_row;
    pstato_partita->sof_col=sof_col;
    pstato_partita->sof_diag=sof_col;

    pstato_partita->pimpostazioni_euristica_ = NULL;

    return (pvoid)pstato_partita;
}

void stato_partita_free(pvoid pstato_partita_)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;

    if (pstato_partita->pimpostazioni_euristica_!=NULL)
    {
        impostazioni_free(pstato_partita->pimpostazioni_euristica_);
    }

    free(pstato_partita);
}

my_int stato_partita_inserisci_pedina(pvoid pstato_partita_, my_uint giocatore, my_uint mossa)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
    int id, riga, colonna;
    unsigned int mask;
    
    if (pstato_partita->paltezza_colonne[mossa]>=pstato_partita->righe)
    {
        return 0;
    }
    
    riga=pstato_partita->righe - 1 - pstato_partita->paltezza_colonne[mossa]++;
    colonna=mossa;
    
    // colonne
    id = DIVIDEBYNCOPPIEPERINT(riga);
    mask = giocatore<<(2 * (NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(riga+1)));
    
    *(pstato_partita->pscacchiera_verticale+pstato_partita->sof_col*(colonna)+id) ^= mask;
    
    // righe
    id = DIVIDEBYNCOPPIEPERINT(colonna);
    mask = giocatore<<(2 * (NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(colonna+1)));
    
    *(pstato_partita->pscacchiera_orizzontale+pstato_partita->sof_row*riga+id) ^= mask;


    // diagonali "\"
    id = DIVIDEBYNCOPPIEPERINT(riga);
    mask = giocatore<<(2 * (NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(riga+1)));
    // diagonale = #righe-1+col-riga
    *(pstato_partita->pscacchiera_diagonale1+pstato_partita->sof_diag*(pstato_partita->righe-1+colonna-riga)+id) ^= mask;

  
    // diagonali "/"
    id = DIVIDEBYNCOPPIEPERINT(riga);
    mask = giocatore<<(2 * (NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(riga+1)));
    // diagonale = col+riga
    *(pstato_partita->pscacchiera_diagonale2+pstato_partita->sof_diag*(colonna+riga)+id) ^= mask;
    
    return 1;
}

void stato_partita_rimuovi_pedina(pvoid pstato_partita_, my_uint giocatore, my_uint mossa)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
    int id, riga, colonna;
    unsigned int mask;
        
    riga=pstato_partita->righe - 1 - (--pstato_partita->paltezza_colonne[mossa]);
    colonna=mossa;
    
    // colonne
    id = DIVIDEBYNCOPPIEPERINT(riga);
    mask = giocatore<<(2 * (NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(riga+1)));
    
    *(pstato_partita->pscacchiera_verticale+pstato_partita->sof_col*colonna+id) ^= mask;
    
    // righe
    id = DIVIDEBYNCOPPIEPERINT(colonna);
    mask = giocatore<<(2 * (NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(colonna+1)));
    
    *(pstato_partita->pscacchiera_orizzontale+pstato_partita->sof_row*riga+id) ^= mask;


    // diagonali "\"
    id = DIVIDEBYNCOPPIEPERINT(riga);
    mask = giocatore<<(2 * (NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(riga+1)));
    // diagonale = #righe-1+col-riga
    *(pstato_partita->pscacchiera_diagonale1+pstato_partita->sof_diag*(pstato_partita->righe-1+colonna-riga)+id) ^= mask;

  
    // diagonali "/"
    id = DIVIDEBYNCOPPIEPERINT(riga);
    mask = giocatore<<(2 * (NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(riga+1)));
    // diagonale = col+riga
    *(pstato_partita->pscacchiera_diagonale2+pstato_partita->sof_diag*(colonna+riga)+id) ^= mask;
    
    return;
}

void stato_partita_fill(pvoid pstato_partita_, my_int val)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
    my_int riga, id;
    
    my_int towrite;
    
    for (towrite=riga=0; riga<NCOPPIEPERINT; towrite=(towrite+val)<<2, riga++);
    
    // scacchiera verticale
    for (id=0; id<((pstato_partita->sof_col)*(pstato_partita->colonne)); id++)
    {
        pstato_partita->pscacchiera_verticale[id]=towrite;
    }
    
    // scacchiera diagonale 1 e 2
    for (id=0; id<((pstato_partita->sof_col)*(pstato_partita->righe+pstato_partita->colonne-1)); id++)
    {
    		 pstato_partita->pscacchiera_diagonale1[id]=pstato_partita->pscacchiera_diagonale2[id]=towrite;
    }
    
    // scacchiera orizzontale
    for (id=0; id<((pstato_partita->sof_row)*(pstato_partita->righe)); id++)
    {
        pstato_partita->pscacchiera_orizzontale[id]=towrite;
    }
}

void stato_partita_randfill(pvoid pstato_partita_)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
    my_int righe, sof_riga, i;
    pchar pid;

    righe = pstato_partita->righe; // numero di righe della _stato_partita
    sof_riga = pstato_partita->sof_row; // numero di int di una riga

    // riempio la scacchiera di celle casuali
    for(pid = (pchar)pstato_partita->pscacchiera_orizzontale, i=0; i<(my_int)(righe*sof_riga*sizeof(my_int)); i++, pid++)
    {
        *pid=rand();
    }
}

void stato_partita_azzera(pvoid pstato_partita_)
 {
  p_stato_partita pstato_partita = (p_stato_partita) pstato_partita_;
  int i;

  stato_partita_fill(pstato_partita, 0);
  for(i=0;i<(pstato_partita->colonne);i++)
   pstato_partita->paltezza_colonne[i] = 0;
 }

void stato_partita_dump(pvoid pstato_partita_)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
    my_int righe, sof_riga, i;
    pmy_int priga;

    righe = pstato_partita->righe; // numero di righe della scacchiera
    sof_riga = pstato_partita->sof_row; // numero di int di una riga
    priga = pstato_partita->pscacchiera_orizzontale;

    printf("Righe: %d\nSof_row: %d\n", righe, sof_riga);

    for(i=0; i<righe*sof_riga; i++, priga++)
    {
        print_dump(priga, sizeof(int));
        printf("\n");
    }
}


// funzione ausiliaria di calcola_features; pfeatures è il puntatore all'int delle features, mentre add è l'intero che bisogna sommare all'indice delle features (per come sono state costruite le cose 0, 256, 512 e 768 rispettivamente per la scacchiera verticale, orizzontale, diagonale "\" e diagonale "/").
void stato_partita_calcola_features_internal(p_stato_partita pstato_partita, my_int colonna_inizio, my_int colonna_fine, my_int riga_inizio, my_int riga_fine, pmy_int pfeatures, my_uint add)
 {
  my_int id, // numero dell'int all'interno della linea da cui iniziare il calcolo delle features
         linee_da_controllare, quartetti_da_controllare_per_linea, quartetti_da_controllare_primo_int, quartetti_da_controllare_per_int, quartetti_da_controllare_fra_int, quartetti_controllati_per_linea, celle_da_non_controllare_primo_int, shift1, shift2, i, j, k, int_da_valutare, sof_linea,
          nrighe, ncolonne, // nuovo numero di righe e di colonne della scacchiera su cui effettuare il controllo
          aus_diag; // variabile ausiliaria per la scacchiera diagonale "/"
  my_uint aus, aus2; // variabili ausiliarie
  pmy_int p_inizio, // puntatore alla prima linea da controllare
          p_int_linea; // puntatore all'int della linea

  nrighe = riga_fine - riga_inizio + 1;
  ncolonne = colonna_fine - colonna_inizio + 1;
  aus_diag = riga_inizio - ncolonne + 1;
  celle_da_non_controllare_primo_int=quartetti_da_controllare_primo_int=quartetti_da_controllare_per_linea=id=0;

  if(add==512 || add==768) // scacchiere diagonali
   {
    linee_da_controllare = ncolonne + nrighe - 1;
    sof_linea = pstato_partita->sof_diag;
    if(add==512) // scacchiera diagonale "\"
     p_inizio = pstato_partita->pscacchiera_diagonale1 + (sof_linea)*(pstato_partita->righe - 1 + colonna_inizio - riga_fine);
    else // scacchiera diagonale "/"
     p_inizio = pstato_partita->pscacchiera_diagonale2 + (sof_linea)*(colonna_inizio + riga_inizio);
   }
  else if(add==256) // scacchiera orizzontale
   {
    sof_linea = pstato_partita->sof_row;
    p_inizio = pstato_partita->pscacchiera_orizzontale + sof_linea*riga_inizio;
    linee_da_controllare = riga_fine - riga_inizio + 1;
    quartetti_da_controllare_per_linea = colonna_fine - colonna_inizio - 2;
    celle_da_non_controllare_primo_int = MODULEBYNCOPPIEPERINT(colonna_inizio);
    quartetti_da_controllare_primo_int = NCOPPIEPERINT - celle_da_non_controllare_primo_int - 3;
    id = DIVIDEBYNCOPPIEPERINT(colonna_inizio);
   }
  else // scacchiera verticale
   {
    sof_linea = pstato_partita->sof_col;
    p_inizio = pstato_partita->pscacchiera_verticale + sof_linea*colonna_inizio;
    linee_da_controllare = colonna_fine - colonna_inizio + 1;
    quartetti_da_controllare_per_linea = riga_fine - riga_inizio - 2;
    celle_da_non_controllare_primo_int = MODULEBYNCOPPIEPERINT(riga_inizio);
    quartetti_da_controllare_primo_int = NCOPPIEPERINT - celle_da_non_controllare_primo_int - 3;
    id = DIVIDEBYNCOPPIEPERINT(riga_inizio);
   }

  shift2 = (sizeof(my_int)-1)*CHAR_BIT;
  p_int_linea = p_inizio;

  // ricordare che quartetti_da_controllare_per_linea e quartetti_da_controllare_primo_int, nelle scacchiere diagonali, cambiano in base alla diagonale
  #if _DEBUG_FEATURES
  printf("Linee da controllare: %d\n", linee_da_controllare);
  printf("\n");
  #endif

  #if _DEBUG_FEATURES_VERT_ORIZZ
  printf("Quartetti da controllare per linea: %d\n", quartetti_da_controllare_per_linea);
  printf("Quartetti da controllare per il primo int: %d\n", quartetti_da_controllare_primo_int);
  printf("\n");
  #endif


  // ciclo su tutte le linee da controllare
  for(i=0, quartetti_controllati_per_linea=0; i<linee_da_controllare; i++, quartetti_controllati_per_linea=0)
   {

    if(add==512 || add==768) // scacchiere diagonali
     {
      // i è il numero della diagonale che sta venendo controllata; si applicano quindi le formule trovate
      if(ncolonne>=nrighe)
       {
        if(i<(nrighe-1)) quartetti_da_controllare_per_linea = i-2; // i+1 - 3
        else if(i>(ncolonne-1)) quartetti_da_controllare_per_linea = nrighe-i+ncolonne-4; // nrighe-i+ncolonne-1 - 3
        else quartetti_da_controllare_per_linea = nrighe-3; // nrighe - 3
       }
      else
       {
        if(i<=(ncolonne-1)) quartetti_da_controllare_per_linea = i-2; // i+1 - 3
        else if(i>=(nrighe-1)) quartetti_da_controllare_per_linea = nrighe-i+ncolonne-4; // nrighe-i+ncolonne-1 - 3
        else quartetti_da_controllare_per_linea = ncolonne-3; // ncolonne - 3
       }

      if(add==512) // scacchiera diagonale "\"
       {
        id = DIVIDEBYNCOPPIEPERINT( ( ((riga_fine-i)>riga_inizio)?(riga_fine-i):(riga_inizio) ) );
        celle_da_non_controllare_primo_int = MODULEBYNCOPPIEPERINT( ( ((riga_fine-i)>riga_inizio)?(riga_fine-i):(riga_inizio) ) );
       }
      else // scacchiera diagonale "/"
       {
        id = DIVIDEBYNCOPPIEPERINT( ( (i<ncolonne)?(riga_inizio):(aus_diag+i) ) );
        celle_da_non_controllare_primo_int = MODULEBYNCOPPIEPERINT( ( (i<ncolonne)?(riga_inizio):(aus_diag+i) ) );
       }
      
      quartetti_da_controllare_primo_int = NCOPPIEPERINT - celle_da_non_controllare_primo_int - 3;

      #if _DEBUG_FEATURES_DIAG
      printf("Quartetti da controllare per linea: %d\n", quartetti_da_controllare_per_linea);
      printf("Quartetti da controllare per il primo int: %d\n", quartetti_da_controllare_primo_int);
      printf("Primo int della linea:\n");
      print_dump(p_int_linea, sizeof(my_int));
      printf("\n");
      #endif

     } // fine blocco per le scacchiere diagonali

    p_int_linea += id; // mi sposto per partire con l'int giusto

    // ciclo su ogni int
    for(j=0; quartetti_controllati_per_linea<quartetti_da_controllare_per_linea; j++)
     {
      #if _DEBUG_FEATURES
      printf("Valutazione dell'int:\n");
      print_dump(p_int_linea, sizeof(my_int));
      #endif

      int_da_valutare = *p_int_linea;
      aus = (my_uint)int_da_valutare;
      if(j==0) // è il primo int della linea
       {
        shift1 = celle_da_non_controllare_primo_int*2;
        quartetti_da_controllare_per_int = quartetti_da_controllare_primo_int;
       }
      else // per tutti gli altri int
       {
        shift1 = 0;
        quartetti_da_controllare_per_int = NCOPPIEPERINT-3;
       }
      
      #if _DEBUG_FEATURES
      printf("--- Valutazione dei quartetti all'interno dell'int ---\n");
      #endif

      // ciclo all'interno dell'int
      for(k=0; (k<quartetti_da_controllare_per_int && quartetti_controllati_per_linea<quartetti_da_controllare_per_linea); k++, quartetti_controllati_per_linea++, shift1+=2, aus = (my_uint)int_da_valutare)
       {
        #if _DEBUG_FEATURES
        printf("Prima dello shift:\n");
        print_dump(&aus, sizeof(my_int));
        #endif
        aus<<=shift1;
        #if _DEBUG_FEATURES
        printf("Dopo il primo shift:\n");
        print_dump(&aus, sizeof(my_int));
        #endif
        aus>>=shift2;
        #if _DEBUG_FEATURES
        printf("Dopo il secondo shift:\n");
        print_dump(&aus, sizeof(my_int));
        printf("Risultato: %d\n", aus+add);
        printf("\n");
        #endif
        pfeatures[(aus+add)]++;
       }


      #if _DEBUG_FEATURES
      printf("--- Valutazione dei quartetti a cavallo fra l'int e il successivo ---\n");
      #endif

      // per le celle a cavallo fra due int
      aus = (my_uint)int_da_valutare; // primo int
      aus<<= shift2; // porto all'inizio dell'int l'ultimo quartetto del primo int
      p_int_linea++;
      aus2 = (my_uint)(*p_int_linea); // secondo int
      aus2>>=8; // porto nel secondo byte il primo quartetto del secondo int
      aus^=aus2; // sovrappongo i due int (mi interessano solo i primi due byte)
      aus2=aus; // adesso aus2 è proprio l'int di cui voglio valutare i primi due byte; mi serve per reimpostare il valore di aus alla fine di ogni iterazione del ciclo

      shift1 = 2;
      quartetti_da_controllare_fra_int = 3;
      if(j==0) // è il primo int, e non è detto che bisogna controllare tutti i quartetti a cavallo fra due int
       {
        if( (NCOPPIEPERINT - celle_da_non_controllare_primo_int) == 2) {shift1 = 4; quartetti_da_controllare_fra_int = 2;}
        if( (NCOPPIEPERINT - celle_da_non_controllare_primo_int) == 1) {shift1 = 6; quartetti_da_controllare_fra_int = 1;}
       }
      for(k=0; (k<quartetti_da_controllare_fra_int && quartetti_controllati_per_linea<quartetti_da_controllare_per_linea); k++, quartetti_controllati_per_linea++, shift1+=2, aus = aus2)
       {
        #if _DEBUG_FEATURES
        printf("Prima dello shift:\n");
        print_dump(&aus, sizeof(my_int));
        #endif
        aus<<=shift1;
        #if _DEBUG_FEATURES
        printf("Dopo il primo shift:\n");
        print_dump(&aus, sizeof(my_int));
        #endif
        aus>>=shift2;
        #if _DEBUG_FEATURES
        printf("Dopo il secondo shift:\n");
        print_dump(&aus, sizeof(my_int));
        printf("Risultato: %d\n", aus+add);
        printf("\n");
        #endif
        pfeatures[(aus+add)]++;
       }

     } // fine del ciclo su ogni int

    // vado alla linea successiva (i è il numero di linee controllate)
    p_int_linea = p_inizio + (i+1)*sof_linea;

   } // fine del ciclo sulle linee
 }

my_int stato_partita_calcola_features(pvoid pstato_partita_, my_int colonna_inizio, my_int colonna_fine, my_int riga_inizio, my_int riga_fine, pmy_int pfeatures)
 {
  p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
  my_int i;

  // sistemo i margini per rimanere all'interno della scacchiera
  if(colonna_inizio < 0) colonna_inizio = 0;
  if(colonna_fine >= pstato_partita->colonne) colonna_fine = (pstato_partita->colonne) - 1;
  if(riga_inizio < 0) riga_inizio = 0;
  if(riga_fine >= pstato_partita->righe) riga_fine = (pstato_partita->righe) -1;

  for(i=0; i<NUMERO_FEATURES; i++) pfeatures[i] = 0;

  stato_partita_calcola_features_internal(pstato_partita, colonna_inizio, colonna_fine, riga_inizio, riga_fine, pfeatures, 0);
  stato_partita_calcola_features_internal(pstato_partita, colonna_inizio, colonna_fine, riga_inizio, riga_fine, pfeatures, 256);
  stato_partita_calcola_features_internal(pstato_partita, colonna_inizio, colonna_fine, riga_inizio, riga_fine, pfeatures, 512);
  stato_partita_calcola_features_internal(pstato_partita, colonna_inizio, colonna_fine, riga_inizio, riga_fine, pfeatures, 768);

  return 1;
 }

my_int stato_partita_euristica_globale(pvoid _pmodello, pvoid pstato_partita_, my_int giocatore)
 {
  my_int i, euristica;
  pmy_int pfeatures, pcoefficienti;
  p_stato_partita pstato_partita;

  pcoefficienti = (pmy_int)_pmodello;
  pstato_partita = (p_stato_partita)pstato_partita_;
  pfeatures = (pmy_int)malloc(NUMERO_FEATURES*sizeof(my_int));

  stato_partita_calcola_features(pstato_partita_, 0, (pstato_partita->colonne-1), 0, (pstato_partita->righe)-1, pfeatures);

  for(i=0, euristica=0; i<NUMERO_FEATURES; i++)
   {euristica += pfeatures[i] * pcoefficienti[i];}

  if(giocatore==2) euristica = - euristica;

  free(pfeatures);
  return euristica;
 }

my_int stato_partita_aggiorna_euristica(pvoid pstato_partita_, pvoid pcoefficienti_, my_int vecchia_euristica, my_uint giocatore_partenza, my_uint giocatore, my_uint mossa)
 {
  p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
  pmy_int pcoefficienti = (pmy_int)pcoefficienti_;

  my_int i, euristica_locale_vecchia, euristica_locale_nuova, riga, colonna, euristica_nuova;
  pmy_int pfeatures;

  riga = pstato_partita->colonne - (pstato_partita->paltezza_colonne)[mossa];
  colonna = mossa;
  pfeatures = (pmy_int)malloc(NUMERO_FEATURES*sizeof(my_int));

  stato_partita_calcola_features(pstato_partita_, colonna-3, colonna+3, riga-3, riga+3, pfeatures);
  for(i=0, euristica_locale_vecchia=0; i<NUMERO_FEATURES; i++)
   {euristica_locale_vecchia += pfeatures[i] * pcoefficienti[i];}

  if (!stato_partita_inserisci_pedina(pstato_partita_, giocatore, mossa))
    {free(pfeatures); return AGGIORNA_EURISTICA_ERRORE;}

  stato_partita_calcola_features(pstato_partita_, colonna-3, colonna+3, riga-3, riga+3, pfeatures);
  for(i=0, euristica_locale_nuova=0; i<NUMERO_FEATURES; i++)
   {euristica_locale_nuova += pfeatures[i] * pcoefficienti[i];}

  if(giocatore_partenza == 1)
  euristica_nuova = (vecchia_euristica - euristica_locale_vecchia + euristica_locale_nuova);
  else
  euristica_nuova = (vecchia_euristica + euristica_locale_vecchia - euristica_locale_nuova);

  free(pfeatures);

  #if AGGIORNA_EURISTICA_ERRORE != INT_MIN
    return (euristica_nuova==AGGIORNA_EURISTICA_ERRORE)?(AGGIORNA_EURISTICA_ERRORE-1):euristica_nuova;
  #else
    return (euristica_nuova==AGGIORNA_EURISTICA_ERRORE)?(AGGIORNA_EURISTICA_ERRORE+1):euristica_nuova;
  #endif
 }

my_int stato_partita_check_vittoria(pvoid pstato_partita_)
 {
  my_int righe, colonne, quartetti_giocatore1, quartetti_giocatore2, celle_riempite, i;
  pmy_int pfeatures, paltezza_colonne;
  p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;

  righe = pstato_partita->righe;
  colonne = pstato_partita->colonne;
  paltezza_colonne = pstato_partita->paltezza_colonne;
  pfeatures = (pmy_int)malloc(NUMERO_FEATURES*sizeof(my_int));
  stato_partita_calcola_features(pstato_partita_, 0, colonne-1, 0, righe-1, pfeatures);
  quartetti_giocatore1 = pfeatures[85] + pfeatures[341] + pfeatures[597] + pfeatures[853];
  quartetti_giocatore2 = pfeatures[170] + pfeatures[426] + pfeatures[682] + pfeatures[938];
  free(pfeatures);  
  
  if(quartetti_giocatore1>0) return 1;
  if(quartetti_giocatore2>0) return 2;

  for(i=0, celle_riempite=0;i<colonne;i++) celle_riempite += paltezza_colonne[i];
  if(celle_riempite>=(righe*colonne)) return (-1);

  return 0;
 }

pvoid stato_partita_pcoefficienti_init()
{
    pvoid pcoefficienti = malloc(NUMERO_FEATURES*sizeof(my_int));
    return pcoefficienti;
}

void stato_partita_pcoefficienti_free(pvoid pcoefficienti_)
{
    free(pcoefficienti_);
}

void stato_partita_pcoefficienti_set_filename(pvoid pstato_partita_, pchar impostazioni_euristica_filename)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
    pvoid plista_campi;
    my_int counter;

    if (pstato_partita->pimpostazioni_euristica_ == NULL)
    {
        plista_campi = lista_campi_init();

        for (counter=0; counter<NUMERO_FEATURES; counter++)
        {
            lista_campi_add(plista_campi, counter, sizeof(my_int), 0);
        }
        pvoid pres=impostazioni_init(plista_campi, impostazioni_euristica_filename);
        pstato_partita->pimpostazioni_euristica_ = pres;
    }
    else
    {
        impostazioni_set_filename(pstato_partita->pimpostazioni_euristica_, impostazioni_euristica_filename);
    }
}

my_int stato_partita_pcoefficienti_load(pvoid pstato_partita_, pvoid pcoefficienti_)
{
    p_stato_partita pstato_partita = (p_stato_partita) pstato_partita_;

    if (pstato_partita->pimpostazioni_euristica_ == NULL) return 0;

    return impostazioni_load(pstato_partita->pimpostazioni_euristica_, pcoefficienti_);
}

my_int stato_partita_pcoefficienti_save(pvoid pstato_partita_, pvoid pcoefficienti_)
{
    p_stato_partita pstato_partita = (p_stato_partita) pstato_partita_;

    if (pstato_partita->pimpostazioni_euristica_ == NULL) return 0;

    return impostazioni_save(pstato_partita->pimpostazioni_euristica_, pcoefficienti_);
}

void stato_partita_pcoefficienti_randfill(pvoid pstato_partita_, pvoid pcoefficienti_)
{
  pmy_int pcoefficienti = (pmy_int)pcoefficienti_;

  my_int i, random;

  for(i=0; i<NUMERO_FEATURES; i++)
  {
   random = rand()%101;
   if(rand()%2) pcoefficienti[i] = random; else pcoefficienti[i] = -random;
  }
}

void stato_partita_pcoefficienti_copy(pvoid pstato_partita_, pvoid pcoefficienti_in_, pvoid pcoefficienti_out_)
{
    memcpy(pcoefficienti_out_, pcoefficienti_in_, sizeof(my_int)*NUMERO_FEATURES);
}

my_int stato_partita_get(pvoid pstato_partita_, my_int row, my_int column)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;

    pmy_uint pint = (pmy_uint)(pstato_partita->pscacchiera_orizzontale) + (pstato_partita->sof_row)*row + DIVIDEBYNCOPPIEPERINT(column);
    my_int shift = 2*(NCOPPIEPERINT - MODULEBYNCOPPIEPERINT(column+1));

    return ((*pint)>>shift) & 0b11;
}

my_int stato_partita_getW(pvoid pstato_partita_)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
    
    return pstato_partita->colonne;
}

my_int stato_partita_getH(pvoid pstato_partita_)
{
    p_stato_partita pstato_partita = (p_stato_partita)pstato_partita_;
    
    return pstato_partita->righe;
}
