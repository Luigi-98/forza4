#ifndef _STATO_PARTITA
#define _STATO_PARTITA

#include "../../../../include/my_defines.h"

#define _SIZEOF_INT_IS_POWEROFTWO 1
#define _CHAR_BITS_IS_POWEROFTWO 1

#define EURISTICA_PUNTEGGIO_VITTORIA INT_MAX
#define EURISTICA_PUNTEGGIO_PERDITA INT_MIN

#define AGGIORNA_EURISTICA_ERRORE   (INT_MIN+1)


// stato_partita_init:   costruttore di stato_partita
// righe:                numero di righe della scacchiera
// colonne:              numero di colonne della scacchiera
// return:               puntatore alla struct
pvoid stato_partita_init(my_uint righe, my_uint colonne);


// stato_partita_free:   distruttore di stato_partita
// pstato:               puntatore alla struct stato_partita da distruggere
void stato_partita_free(pvoid pstato_partita_);


// inserisci_pedina:   inserisce una pedina nella scacchiera
// pstato:             puntatore alla struct con la scacchiera
// giocatore:          tipo della pedina da inserire (1 o 2)
// mossa:              colonna nella quale inserire la pedina
// return:             1 vero se la pedina è stata inserita, 0 se la mossa era illegale
my_int stato_partita_inserisci_pedina(pvoid pstato_partita_, my_uint giocatore, my_uint mossa);


// rimuovi_pedina:   rimuove una pedina dalla scacchiera
// pstato:           puntatore alla struct con la scacchiera
// giocatore:        tipo della pedina da rimuovere (1 o 2)
// mossa:            colonna dalla quale rimuovere la pedina
void stato_partita_rimuovi_pedina(pvoid pstato_partita_, my_uint giocatore, my_uint mossa);


// fill:     riempie tutti gli int della scacchiera con il valore val (0, 1 o 2)
// pstato:   puntatore alla struct con la scacchiera
// val:      valore da usare per il riempimento della scacchiera
void stato_partita_fill(pvoid pstato_partita_, my_int val);


// randfill:   riempie tutti gli int della scacchiera con i valori 0, 1, 2, 3 scelti a caso; necessita srand
// pstato:     puntatore alla struct con la scacchiera
void stato_partita_randfill(pvoid pstato_partita_);


// azzera:   rimuove tutte le pedine dalla scacchiera e azzera l'altezza delle colonne
// pstato:   puntatore alla struct con la scacchiera
void stato_partita_azzera(pvoid pstato_partita_);


// dump:     stampa tutti i bit della scacchiera orizzontale
// pstato:   puntatore alla struct con la scacchiera
void stato_partita_dump(pvoid pstato_partita_);


// calcola_features:   calcola le features e le salva in un array, in successione nelle 4 direzioni: verticale, orizzontale, diagonale "\" e diagonale "/"
// psato:              puntatore alla struct con la scacchiera
// colonna_inizio:     colonna dalla quale iniziare il cacolo delle features
// colonna_fine:       colonna nella quale finire il cacolo delle features
// riga_inizio:        riga dalla quale iniziare il cacolo delle features
// riga_fine:          riga nella quale finire il cacolo delle features
// pfeatures:          puntatore all'array che conterrà le features; la sua lunghezza deve essere almeno pari al numero di features
// return:             1 in caso di successo, 0 in caso di fallimento
my_int stato_partita_calcola_features(pvoid pstato_partita_, my_int colonna_inizio, my_int colonna_fine, my_int riga_inizio, my_int riga_fine, pmy_int pfeatures);


// euristica_globale:   calcola il valore dell'euristica di una situazione di gioco, contando tutte le features della scacchiera
// pmodello:            puntatore al modello da usare per il calcolo dell'euristica (ovvero, i coefficienti)
// pstato:              puntatore alla struct con la scacchiera
// giocatore:           giocatore (1 o 2) per il quale calcolare l'euristica
// return:              valore dell'euristica
my_int stato_partita_euristica_globale(pvoid pmodello_, pvoid pstato_partita_, my_int giocatore);


// aggiorna_euristica:    aggiunge una pedina e aggiorna il valore dell'euristica alla nuova situazione
// pstato:                puntatore alla struct con la scacchiera
// pmodello:              puntatore al modello da usare per il calcolo dell'euristica (ovvero, i coefficienti)
// vecchia_euristica:     valore dell'euristica prima dell'aggiunta della pedina
// giocatore: partenza:   giocatore (1 o 2) per il quale calcolare l'euristica
// giocatore:             tipo della pedina da inserire (1 o 2)
// mossa:                 colonna nella quale inserire la pedina
my_int stato_partita_aggiorna_euristica(pvoid pstato_partita_, pvoid pcoefficienti_, my_int vecchia_euristica, my_uint giocatore_partenza, my_uint giocatore, my_uint mossa);



// check_vittoria:   verifica se uno dei due giocatori ha vinto
// psato:            puntatore alla struct con la scacchiera
// return:           numero del giocatore vincente (1 o 2) se qualcuno ha vinto, -1 se la partita è finita in pareggio, 0 altrimenti
my_int stato_partita_check_vittoria(pvoid pstato_partita_);


// pcoefficienti_init:   costruttore dell'array con i coefficienti
// return:               puntatore all'array con i coefficienti
pvoid stato_partita_pcoefficienti_init();


// pcoefficienti_free:   distruttore dell'array con i coefficienti
// pcoefficienti:        puntatore all'array da distruggere
void stato_partita_pcoefficienti_free(pvoid pcoefficienti_);

// pcoefficienti_set_filename:      va chiamato una volta prima di effettuare una
//                                  qualunque operazione di salvataggio o caricamento
// pstato_partita_:                 oggetto di tipo stato_partita
// impostazioni_euristica_filename: nome del file su cui salvare i risultati dell'apprendimento
void stato_partita_pcoefficienti_set_filename(pvoid pstato_partita_, pchar impostazioni_euristica_filename);

// pcoefficienti_load/save:         salva o carica i risultati dell'apprendimento
//                                  all'indirizzo specificato dal parametro pcoefficienti_
// pcoefficienti_:                  indirizzo su cui caricare/da cui salvare i dati
my_int stato_partita_pcoefficienti_load(pvoid pstato_partita_, pvoid pcoefficienti_);
my_int stato_partita_pcoefficienti_save(pvoid pstato_partita_, pvoid pcoefficienti_);

// pcoefficienti_randfill:          inizializza casualmente i parametri da imparare
// pcoefficienti_:                  indirizzo in cui scriverli
void stato_partita_pcoefficienti_randfill(pvoid pstato_partita_, pvoid pcoefficienti_);

// pcoefficienti_copy:              copia i parametri appresi da un indirizzo a un altro
// pcoefficienti_in_:               indirizzo di partenza
// pcoefficienti_out_:              indirizzo di arrivo
void stato_partita_pcoefficienti_copy(pvoid pstato_partita_, pvoid pcoefficienti_in_, pvoid pcoefficienti_out_);

// allena:                     il computer gioca contro sè stesso, generando n-uple casuali di coefficienti contro cui scontrarsi; se una n-upla perde, viene sostituita con un'altra generata casualmente
// pstato:                     puntatore alla struct con la scacchiera
// minimax:                    puntatore alla struct minimax
// numero_partite_per_nupla:   numero di partite da fare prima di decretare un n-upla vincente
// numero_nuple_da_generare:   numero di n-uple generate casualmente che devono scontrarsi
// pcoefficienti_vincenti:     puntatore all'array in cui salvare i coefficienti vincenti
void stato_partita_allena(pvoid pstato_partita_, pvoid pminimax_, my_int numero_partite_per_nupla, my_int numero_nuple_da_generare, pvoid _pcoefficienti_vincenti);

// get:         permette di estrarre il contenuto della cella indicata
// row/column   coordinate della cella
//
// returns 0 (vuota), 1, 2 o 3 (giocatore 1, 2 o 3)
my_int stato_partita_get(pvoid pstato_partita_, my_int row, my_int column);

// getH/getW:       permettono di estrarre l'altezza e la larghezza della scacchiera
my_int stato_partita_getH(pvoid pstato_partita_);
my_int stato_partita_getW(pvoid pstato_partita_);

#endif
