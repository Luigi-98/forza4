#ifndef _STATO_PARTITA_HIDDEN
#define _STATO_PARTITA_HIDDEN

#include "stato_partita.h"

typedef pmy_int modello;
typedef modello* pmodello;

typedef struct __stato_partita
 {
  pmy_int pscacchiera_verticale,
     pscacchiera_orizzontale,
     pscacchiera_diagonale1,
     pscacchiera_diagonale2;
  pmy_int paltezza_colonne;
  my_int sof_row, sof_col, sof_diag, righe, colonne;
  
  pvoid pimpostazioni_euristica_;
 } _stato_partita;

typedef _stato_partita* p_stato_partita;

#endif
