#ifndef _FORZA4_TIPI_PUNTATORI_FUNZIONI_H
#define _FORZA4_TIPI_PUNTATORI_FUNZIONI_H

#include "../../../include/my_defines.h"

/*
 * Permette di stampare l'attuale stato della partita.
 * 
 * returns          vero se la stampa è andata a buon fine
 * */
typedef my_int (*p_print_stato_partita_t)(pvoid pinterfaccia, pvoid pstato_partita);
/*
 * Permette di acquisire una nuova mossa per il giocatore specificato.
 * 
 * pstato_partita       oggetto di tipo stato_partita
 * giocatore            il giocatore di cui acquisire la mossa
 * 
 * returns          il numero della colonna corrispondente alla mossa fatta [1, n_colonne]
 *                  -1 se il giocatore ha espresso la preferenza di terminare la partita
 *                  0 se è stata acquisita una mossa non valida
 * */
typedef my_int (*p_scanf_mossa_t)(pvoid pinterfaccia_, pvoid pstato_partita, my_int giocatore);

/*
 * Permette di mettere in pausa il programma.
 * 
 * Viene usata con show_message e hide_message per permettere la lettura.
 * */
typedef void (*p_wait_t)();

/*
 * Permettono di mostrare (e successivamente nascondere) un messaggio di una riga.
 * 
 * txt          il testo da mostrare (const char* o char*)
 * 
 * returns      vero se mostrato/nascosto con successo
 * */
typedef my_int (*p_print_show_message_t)(pvoid pinterfaccia_, pchar txt);
typedef my_int (*p_print_hide_message_t)(pvoid pinterfaccia_);

/*
 * Permettono di mostrare in tempo reale (e successivamente nascondere) un output
 * di tipo log multilinea.
 * Può essere chiamata più volte prima di utilizzare print_hide_big.
 * 
 * txt          la riga/le righe da aggiungere (le righe vanno separate da '\n',
 *              per lasciare una riga vuota aggiungere " \n")
 * 
 * returns      vero se mostrato/nascosto con successo
 * */
typedef my_int (*p_print_big_t)(pvoid pinterfaccia_, pchar txt);
typedef my_int (*p_print_hide_big_t)(pvoid pinterfaccia_);

/*
 * Permettono di stampare (e successivamente nascondere) una percentuale.
 * (utilizzata in allenamento contemporaneamente a print_big)
 * 
 * percentuale      indica la percentuale come intero tra 0 e 100
 * 
 * returns      vero se mostrata/nascosta con successo
 * */
typedef my_int (*p_print_percentage_t)(pvoid pinterfaccia_, my_int percentuale);
typedef my_int (*p_print_percentage_hide_t)(pvoid pinterfaccia_);

#endif