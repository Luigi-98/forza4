my_int print_scacchiera(pstato_partita pstato)
{
  if(pstato==NULL) return 0;

  char contenuto_celle[] = {'_', 'X', 'O', 'T'};
  my_int cella, i, k, celle_scritte_per_riga, shift, sof_row, righe, colonne;
  my_uint mask, int_riga;
  pmy_int priga; // puntatore a un int della riga

  mask = 0b11;
  righe = pstato->righe; // numero di righe della scacchiera
  colonne = pstato->colonne; // numero di colonne della scacchiera
  sof_row = pstato->sof_row; // numero di int di una riga
  priga = pstato->pscacchiera_orizzontale;

  /** crea lato superiore del riquadro **/
  for(i=0; i<colonne; i++)
   {
    printf("---");
   }
  printf("----\n");

  /** crea parte centrale del riquadro **/
  // ciclo su ogni int della scacchiera
  // l'indice parte da 1 per evitare il problema con i=0 nel capire se la riga è finita
  for(i=1, celle_scritte_per_riga=0, priga=pstato->pscacchiera_orizzontale; i<=righe*sof_row; i++, priga++)
   {
     int_riga = *priga;

     // il primo shift è della lunghezza dell'int meno 2 bit
     shift = sizeof(my_int)*CHAR_BIT - 2; // =(sizeof(my_int)-1)*CHAR_BIT + (CHAR_BIT-2);

     // crea lato sinistro del riquadro
     if(celle_scritte_per_riga==0) printf("| ");

     // ciclo su ogni coppia di bit dell'int
     for  (k=0;
            (k<(my_int)NCOPPIEPERINT) && (celle_scritte_per_riga<colonne);
            k++, shift-=2, celle_scritte_per_riga++)
      {
       cella = (int_riga >> shift) & mask;
       printf(" %c ", contenuto_celle[cella]);
      }
      
     // crea lato destro del riquadro e passa alla riga successiva (è finita la riga)
     if(i%sof_row == 0)
      {
       printf(" |\n");
       celle_scritte_per_riga = 0;
      }
   }

  /** crea lato inferiore del riquadro **/
  for(i=0; i<colonne; i++)
   {
    printf("---");
   }
  printf("----\n");


  return 1;
}