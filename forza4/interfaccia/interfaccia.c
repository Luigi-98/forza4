#include <stdio.h>
#include <limits.h>

#include <time.h>
#include <stdlib.h>
#include <string.h>

#ifndef _INTERFACCIA_OLD
#include "interfaccia.core.h"
#else
#include "interfaccia.core.old.h"
#endif

#include "interfaccia.h"
#include "interfaccia.hidden.h"
#include "forza4/forza4.h"
#include "../../include/my_defines.h"
#include "../../include/utils/math_utils.h"

my_int interfaccia_settings_menu(pvoid pinterfaccia_, pvoid pforza4_)
{
  p_interfaccia pinterfaccia = (p_interfaccia)pinterfaccia_;

  char dimscacchiera[40];
  char profallenamento[40];
  pchar menuitems[]={ dimscacchiera,
                      profallenamento,
                      "Reset",
                      "Indietro"};
  int n_items=4;
  int counter, val;
  int input0, input1;
  int x=3, y=2;
  int maxlen=MIN_W_MENU_WIN-4;

  int settings_changed = 0;

  char txt_out[150];
  
  start_menu:
  sprintf(dimscacchiera, "Dimensione della scacchiera (%dx%d)", forza4_getH(pforza4_), forza4_getW(pforza4_));
  sprintf(profallenamento, "Difficolta' (%d)", forza4_get_difficulty(pforza4_));

  for (counter=0; counter<n_items; counter++)
  {
    maxlen = MAX(maxlen, (int)strlen(menuitems[counter]));
  }
  
  interfaccia_resize_pwin(pinterfaccia, maxlen+2*x, n_items+2*y);

  val=interfaccia_textmenu(pinterfaccia->pwin, menuitems, n_items, x, y);

  switch (val)
  {
    case 0:
      sprintf(txt_out, "Inserire 0 per mantenere invariata l'impostazione");
      interfaccia_show_message(pinterfaccia_, txt_out);
      interfaccia_wait();
      interfaccia_hide_message(pinterfaccia_);
      input1 = interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Inserisci il numero di righe: ", 0);
      input0 = interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Inserisci il numero di colonne: ", 0);
      
      if (input0 == 0)
      {
        input0 = forza4_getW(pforza4_);
      }
      else
      {
        settings_changed=1;
      }

      if (input1 == 0)
      {
        input1 = forza4_getH(pforza4_);
      }
      else
      {
        settings_changed=1;
      }
      
      if (settings_changed)
        if (!forza4_set_size(pforza4_, input1, input0))
        {
          interfaccia_show_message(pinterfaccia_, "Dimensione della scacchiera non valida");
          interfaccia_wait();
          interfaccia_hide_message(pinterfaccia_);
        }
      break;
    case 1:
      sprintf(txt_out, "Inserire 0 per mantenere invariata l'impostazione");
      interfaccia_show_message(pinterfaccia_, txt_out);
      interfaccia_wait();
      interfaccia_hide_message(pinterfaccia_);

      input0 = interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Inserisci la difficolta': ", 0);
      if (input0 == 0)
      {
        break;
      }

      forza4_set_difficulty(pforza4_, input0);
      settings_changed=1;
      break;
    case 2:
      forza4_impostazioni_default_fill(pforza4_);
      forza4_impostazioni_apply(pforza4_);
      settings_changed=1;
      break;
    case -1:
    case 3:
      if (settings_changed)
      {
        if (forza4_impostazioni_save(pforza4_))
        {
          interfaccia_show_message(pinterfaccia_, "Impostazioni salvate!");
          interfaccia_wait();
          interfaccia_hide_message(pinterfaccia_);
        }
        else
        {
          interfaccia_show_message(pinterfaccia_, "Non sono riuscito a salvare le impostazioni!");
          interfaccia_wait();
          interfaccia_hide_message(pinterfaccia_);
        }
      }
      return 1;
  }
  goto start_menu;
}

my_int interfaccia_allena_menu(pvoid pinterfaccia_, pvoid pforza4_)
{
  p_interfaccia pinterfaccia = (p_interfaccia)pinterfaccia_;
  char cicli[40], nuple[40], partite[40];
  static my_int n_cicli = SUGGESTED_N_CICLI_ALLENAMENTO;
  static my_int n_nuple = SUGGESTED_N_NUPLE_PER_CICLO;
  static my_int n_partite = SUGGESTED_N_PARTITE_PER_NUPLA_ALLENAMENTO;

  pchar menuitems[]={ "Allena",
                      cicli,
                      nuple,
                      partite,
                      "Indietro"};
  int n_items = 5;
  int val, counter;
  int x=3, y=2;
  int maxlen=MIN_W_MENU_WIN-4;
  int input;

  start_menu:
  sprintf(cicli, "Numero di cicli: %d", n_cicli);
  sprintf(nuple, "Numero n-uple per ciclo: %d", n_nuple);
  sprintf(partite, "Numero partite per n-upla: %d", n_partite);

  for (counter=0; counter<n_items; counter++)
  {
    maxlen = MAX(maxlen, (int)strlen(menuitems[counter]));
  }
  
  interfaccia_resize_pwin(pinterfaccia, maxlen+2*x, n_items+2*y);

  val=interfaccia_textmenu(pinterfaccia->pwin, menuitems, n_items, x, y);
  
  switch (val)
  {
    case 0:
      forza4_allena(pforza4_, pinterfaccia_, n_cicli, n_nuple, n_partite);
      break;
    case 1:
      input = interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Inserisci numero di cicli: ", 0);
      if (input) n_cicli = input;
      break;
    case 2:
      input = interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Inserisci numero di n-uple: ", 0);
      if (input) n_nuple = input;
      break;
    case 3:
      input = interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Inserisci numero di partite: ", 0);
      if (input) n_partite = input;
      break;
    case -1:
    case 4:
      return 1;
  }
  goto start_menu;
}

my_int interfaccia_menu(pvoid pinterfaccia_, pvoid pforza4_)
{
  p_interfaccia pinterfaccia = (p_interfaccia)pinterfaccia_;
  pchar menuitems[]={ "Gioca con umano",
                      "Gioca con PC",
                      "Allena PC",
                      "Impostazioni",
                      "Esci"};
  int n_items = 5;
  int val, counter;
  int x=3, y=2;
  int maxlen=MIN_W_MENU_WIN-4;

  for (counter=0; counter<n_items; counter++)
  {
    maxlen = MAX(maxlen, (int)strlen(menuitems[counter]));
  }
  
  start_menu:
  interfaccia_resize_pwin(pinterfaccia, maxlen+2*x, n_items+2*y);

  val=interfaccia_textmenu(pinterfaccia->pwin, menuitems, n_items, x, y);
  
  switch (val)
  {
    case 0:
      forza4_gioca_vs_umano(pforza4_, pinterfaccia_);
      break;
    case 1:
      forza4_gioca_vs_pc(pforza4_, pinterfaccia_);
      break;
    case 2:
      interfaccia_allena_menu(pinterfaccia_, pforza4_);
      break;
    case 3:
      interfaccia_settings_menu(pinterfaccia_, pforza4_);
      break;
    case -1:
    case 4:
      return EXIT_SUCCESS;
  }
  goto start_menu;
}