#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "interfaccia.h"
#include "interfaccia.hidden.h"

#include "../../include/queue/queue.h"
#include "../../include/utils/math_utils.h"
#include "../../include/utils/time_utils.h"

#define MAX_SCANF_INT 15
#define MIN_W_BIG_WIN 10
#define MIN_H_BIG_WIN 10
#define MIN_W_MENU_WIN 10
#define WIDTH_PERCENTAGE_BAR 20
#define STD_PWIN_SIZE 10

#define SCANF_BIGJUMP 4

#define INTERFACCIA_DEFAULT_WAIT 2

#define MY_KEY_CBREAK 3
#define MY_KEY_BACKSPACE  127
#define MY_KEY_ESCAPE 27


my_int interfaccia_set_title(pvoid pinterfaccia_, pchar title)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;

  if (title!=NULL)
  {
    if (pinterfaccia->title!=NULL)
    {
      free(pinterfaccia->title);
    }
    pinterfaccia->title = (pchar)malloc(strlen(title)+1);
    strcpy(pinterfaccia->title, title);
  }

  return 0;
}

void interfaccia_clear_screen(pvoid pinterfaccia_)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;

  clear();
  refresh();
  wclear(pinterfaccia->pwin);
  box(pinterfaccia->pwin, 0, 0);
  mvwprintw(pinterfaccia->pwin, 0, 1, "%s", pinterfaccia->title);
  wrefresh(pinterfaccia->pwin);
}

void interfaccia_resize_pwin(p_interfaccia pinterfaccia, my_int w, my_int h)
{
  my_int scnw, scnh;

  getmaxyx(stdscr, scnh, scnw);

  wresize(pinterfaccia->pwin, h, w);

  mvwin(pinterfaccia->pwin, (scnh-h)/2, (scnw-w)/2);
  interfaccia_clear_screen(pinterfaccia);
}

pvoid interfaccia_init(pchar title)
{
  p_interfaccia pinterfaccia = (p_interfaccia) malloc(sizeof(_interfaccia));
  my_int scnw, scnh;

  if (pinterfaccia==NULL)
  {
    return NULL;
  }

  initscr();
  assume_default_colors(-1,-1);

  cbreak();
  raw();
  noecho();
  curs_set(0);

  getmaxyx(stdscr, scnh, scnw);

  pinterfaccia->pwin=newwin(STD_PWIN_SIZE, STD_PWIN_SIZE, (scnh-STD_PWIN_SIZE)/2, (scnw-STD_PWIN_SIZE)/2);
  if (pinterfaccia->pwin==NULL)
  {
    free(pinterfaccia);
    endwin();
    return NULL;
  }

  pinterfaccia->pmessagewin=newwin(1, scnw, getbegy(pinterfaccia->pwin)-2, 0);
  if (pinterfaccia->pmessagewin==NULL)
  {
    delwin(pinterfaccia->pwin);
    free(pinterfaccia);
    endwin();
    return NULL;
  }
  
  pinterfaccia->pbigmessagewin=newwin(pinterfaccia->bigmessagewin_h = MAX(MIN_H_BIG_WIN, scnh/2),
                                      pinterfaccia->bigmessagewin_w = MAX(MIN_W_BIG_WIN, scnw/2),
                                      scnh/4,
                                      scnw/4);
  if (pinterfaccia->pbigmessagewin==NULL)
  {
    delwin(pinterfaccia->pmessagewin);
    delwin(pinterfaccia->pwin);
    free(pinterfaccia);
    endwin();
    return NULL;
  }

  pinterfaccia->pbigmessagewincontentqueue = queue_init();
  if (pinterfaccia->pbigmessagewincontentqueue==NULL)
  {
    delwin(pinterfaccia->pbigmessagewin);
    delwin(pinterfaccia->pmessagewin);
    delwin(pinterfaccia->pwin);
    free(pinterfaccia);
    endwin();
    return NULL;
  }

  pinterfaccia->title=NULL;
  interfaccia_set_title(pinterfaccia, title);
  interfaccia_clear_screen(pinterfaccia);

  return pinterfaccia;
}

void interfaccia_free(pvoid pinterfaccia_)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;

  if (pinterfaccia->title!=NULL) free(pinterfaccia->title);
  
  while (queue_len(pinterfaccia->pbigmessagewincontentqueue))
  {
    free(queue_get(pinterfaccia->pbigmessagewincontentqueue));
  }
  queue_free(pinterfaccia->pbigmessagewincontentqueue);
  delwin(pinterfaccia->pbigmessagewin);
  delwin(pinterfaccia->pwin);
  delwin(pinterfaccia->pmessagewin);
  use_default_colors();
  endwin();

  free(pinterfaccia_);
}

my_int interfaccia_textmenu(pwindow pwin, ppchar pitems, my_int n_items, my_int x, my_int y)
{
  my_int counter;
  my_int c;
  static my_int item=0;

  if (item>=n_items) item=n_items-1;

  keypad(pwin, true);
  do
  {
    for (counter=0; counter<n_items; counter++)
    {
      if (counter==item)
        wattron(pwin, A_STANDOUT);
      wmove(pwin, counter+y, x);
      wprintw(pwin, "%s", pitems[counter]);
      wattroff(pwin, A_STANDOUT);
    }
    wrefresh(pwin);
    c=wgetch(pwin);

    switch (c)
    {
      case KEY_UP:
        if (item>0)
          item--;
        break;
      case KEY_DOWN:
        if (item<n_items-1)
          item++;
        break;
      case MY_KEY_CBREAK:
        endwin();
        exit(EXIT_FAILURE);
      case MY_KEY_ESCAPE:
        return -1;
    }
  } while (c!=KEY_ENTER && c!='\n');

  return item;
}

my_int interfaccia_print_centered(pwindow pwin, char* txt, my_int width)
{
    my_int len = strlen(txt), count;
    pchar spaces = (pchar) malloc((width-len)/2+1);
    if (spaces==NULL)
      return 0;

    wmove(pwin, getmaxy(pwin)/2, (getmaxx(pwin)-width)/2);
    
    for (count=0; count<(width-len)/2; count++)
        spaces[count]=' ';
    spaces[count]='\0';

    wprintw(pwin, "%s%s%s", spaces, txt, spaces);

    free(spaces);

    return 1;
}

my_int interfaccia_scanf_int_centered(pwindow pwin, pchar txt, my_int width)
{
  if (width==0)
  {
    width=strlen(txt)+10;
  }

  pchar spaces = (pchar) malloc(width/2+1);
  if (spaces==NULL)
    return 0;
  
  pchar text = (pchar) malloc(width);
  if (text == NULL)
    return 0;

  my_int input=0;
  my_int key;

  keypad(pwin, true);
  do
  {
    sprintf(text, "%s%d", txt, input);

    wattron(pwin, A_STANDOUT);
    interfaccia_print_centered(pwin, text, width);
    wattroff(pwin, A_STANDOUT);
    
    key=wgetch(pwin);
    if (IN_RANGE(key, '0', '9'))
    {
      if (input<INT_MAX/10)
        input=input*10+key-'0';
    }
    else
    {
      if ((key=='\n')||(key==KEY_BACKSPACE))
      {
        free(spaces);
        free(text);
        return input;
      }

      if ((key==MY_KEY_BACKSPACE))
        input/=10;
      
      if (key==MY_KEY_CBREAK)
      {
        endwin();
        exit(EXIT_FAILURE);
      }
    }
  } while (1);

  free(spaces);
  free(text);

  return input;
}

my_int interfaccia_print_scacchiera(pvoid pinterfaccia_, pvoid pforza4_)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;

  pwindow pwin = pinterfaccia->pwin;

  my_int riga, colonna;
  my_int righe = forza4_getH(pforza4_);
  my_int colonne = forza4_getW(pforza4_);

  interfaccia_resize_pwin(pinterfaccia, 3*colonne+4, righe+2);

  for (riga=0; riga<righe; riga++)
  {
    wmove(pwin, riga+1, 2);
    for (colonna=0; colonna<colonne; colonna++)
    {
      wprintw(pwin, " %c ", contenuto_celle[forza4_get_cell(pforza4_, riga, colonna)]);
    }
  }

  interfaccia_refresh(pinterfaccia_, 0);

  return 1;
}

my_int interfaccia_scanf_mossa(pvoid pinterfaccia_, pvoid pforza4_, my_int giocatore)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;
  
  static my_int mossa=1;
  
  //interfaccia_show_message(pinterfaccia_, "Mossa:");
  
  //wprintw(pinterfaccia->pmessagewin, "Mossa giocatore %d: ", giocatore);
  
  //wscanw(pinterfaccia->pmessagewin, "%d", &mossa);
  //mossa=interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Mossa: ", 15);

  pwindow pwin = pinterfaccia->pwin;

  my_int c;
  
  my_int riga, colonna;
  my_int righe = forza4_getH(pforza4_);
  my_int colonne = forza4_getW(pforza4_);

  interfaccia_resize_pwin(pinterfaccia, 3*colonne+4, righe+2);

  if (mossa>colonne) mossa=1;

  keypad(pwin, true);
  do
  {
    for (riga=0; riga<righe; riga++)
    {
      wmove(pwin, riga+1, 2);
      for (colonna=0; colonna<colonne; colonna++)
      {
        if (colonna==mossa-1) wattron(pwin, A_STANDOUT);
        wprintw(pwin, " %c ", contenuto_celle[forza4_get_cell(pforza4_, riga, colonna)]);
        wattroff(pwin, A_STANDOUT);
      }
    }

    c=wgetch(pwin);

    switch (c)
    {
      case KEY_LEFT:
        if (mossa>1)
          mossa--;
        break;
      case KEY_RIGHT:
        if (mossa<colonne)
          mossa++;
        break;
      case KEY_SRIGHT:
        mossa = MIN(mossa+SCANF_BIGJUMP, colonne);
        break;
      case KEY_SLEFT:
        mossa = MAX(mossa-SCANF_BIGJUMP, 1);
        break;
      case KEY_END:
        mossa = colonne;
        break;
      case KEY_HOME:
        mossa = 1;
        break;
      case MY_KEY_CBREAK:
        endwin();
        exit(EXIT_FAILURE);
      case MY_KEY_ESCAPE:
        return -1;
    }
  } while (c!=KEY_ENTER && c!='\n');

  interfaccia_refresh(pinterfaccia_, 0);

  return (IN_RANGE(mossa, 1, colonne))?mossa:0;
}

void interfaccia_refresh(pvoid pinterfaccia_, my_int message)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;

  wrefresh(pinterfaccia->pwin);
  if (message)
    wrefresh(pinterfaccia->pmessagewin);

  refresh();
}

my_int interfaccia_show_message(pvoid pinterfaccia_, pchar message)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;
  pwindow pwin = pinterfaccia->pmessagewin;
  my_int width;
  pchar pid, pcopy = (pchar) malloc(strlen(message));

  for (pid=message; *pid!='\n'&&*pid!='\0'; pid++);

  memcpy(pcopy, message, pid-message);
  *(pcopy+(pid-message))='\0';

  width=strlen(message)+10;
  if (width<=getmaxx(pinterfaccia->pwin))
  {
    width = getmaxx(pinterfaccia->pwin)+10;
  }
  
  wattron(pwin, A_STANDOUT);
  interfaccia_print_centered(pwin, message, width);
  wattroff(pwin, A_STANDOUT);
  interfaccia_refresh(pinterfaccia_, 1);

  free(pcopy);

  return 1;
}

my_int interfaccia_hide_message(pvoid pinterfaccia_)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;

  wclear(pinterfaccia->pmessagewin);

  touchwin(pinterfaccia->pwin);
  
  interfaccia_refresh(pinterfaccia_, 1);

  return 0;
}
my_int interfaccia_show_percentage(pvoid pinterfaccia_, my_int percentage)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;
  pwindow pwin = pinterfaccia->pmessagewin;
  my_int count, n_yes = percentage*WIDTH_PERCENTAGE_BAR/100;
  my_int n_no = WIDTH_PERCENTAGE_BAR-n_yes;

  pchar spaces = (pchar) malloc(MAX(n_yes, n_no)+1);
  if (spaces==NULL)
    return 0;
  
  interfaccia_hide_message(pinterfaccia_);

  wmove(pwin, getmaxy(pwin)/2, (getmaxx(pwin)-WIDTH_PERCENTAGE_BAR)/2);
  
  for (count=0; count<n_yes; count++)
      spaces[count]=' ';
  spaces[count]='\0';

  wprintw(pwin, "|");
  wattron(pwin, A_STANDOUT);
  wprintw(pwin, "%s", spaces);
  wattroff(pwin, A_STANDOUT);

  for (count=0; count<n_no; count++)
      spaces[count]='_';
  spaces[count]='\0';
  
  wprintw(pwin, "%s|  %d%%", spaces, percentage);

  free(spaces);

  interfaccia_refresh(pinterfaccia_, 1);

  return 1;
}

my_int interfaccia_show_bigmessage(pvoid pinterfaccia_)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;
  pwindow pwin = pinterfaccia->pbigmessagewin;
  pwindow pmessage = pinterfaccia->pmessagewin;
  pvoid pcontent = pinterfaccia->pbigmessagewincontentqueue;
  my_int n_rows = queue_len(pcontent), counter;

  if (getbegy(pmessage)>getbegy(pwin))
    mvwin(pmessage, getbegy(pwin)-2, 0);
  wclear(pwin);
  box(pwin, 0, 0);
  for (counter=0; counter<n_rows; counter++)
  {
    mvwprintw(pwin, counter+1, 2, "%s", queue_get_by_id(pcontent, counter));
  }
  wrefresh(pwin);
  wrefresh(pmessage);

  return counter;
}

my_int interfaccia_hide_bigmessage(pvoid pinterfaccia_)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;

  mvwin(pinterfaccia->pmessagewin, getbegy(pinterfaccia->pwin)-2, 0);
  wclear(pinterfaccia->pbigmessagewin);

  touchwin(pinterfaccia->pwin);
  
  wrefresh(pinterfaccia->pbigmessagewin);
  interfaccia_refresh(pinterfaccia_, 1);

  return 0;
}

my_int interfaccia_println_bigmessage(pvoid pinterfaccia_, pchar txt)
{
  p_interfaccia pinterfaccia = (p_interfaccia) pinterfaccia_;
  pvoid pcontent = pinterfaccia->pbigmessagewincontentqueue;
  my_int n_rows = queue_len(pcontent), add_res;
  pchar newrow;
  pchar pid;

  if (n_rows>=pinterfaccia->bigmessagewin_h-2) free(queue_get(pcontent));

  for (pid=txt; *pid!='\n'&&*pid!='\0'&&(pid-txt<pinterfaccia->bigmessagewin_w-4); pid++);

  newrow=(pchar)malloc(pid-txt+1);
  if (newrow==NULL) return 0;

  memcpy(newrow, txt, pid-txt);
  newrow[pid-txt]='\0';

  add_res = queue_add(pcontent, newrow);

  if (*pid == '\n' && *(pid+1)!='\0')
  {
    interfaccia_println_bigmessage(pinterfaccia_, pid+1);
  }

  return add_res;
}

my_int interfaccia_fastprintln_bigmessage(pvoid pinterfaccia_, pchar txt)
{
  interfaccia_println_bigmessage(pinterfaccia_, txt);
  return interfaccia_show_bigmessage(pinterfaccia_);
}

void interfaccia_wait()
{
  //time_delay(INTERFACCIA_DEFAULT_WAIT);
  getch();
}

/*
 * NCURSES:
 * ========
 * 
 * https://www.youtube.com/watch?v=DtMfkShh03w
 * 
 * initscr()            inizializza ncurses
 * endwin()             fa free di ncurses
 * 
 * getch()              equivalente di getchar()
 * printw(cose)         equivalente di printf(cose)
 * 
 * clear()              cls
 * 
 * move(y,x)            imposta la posizione a (x,y)
 * mvprintw(y,x, cose)  equivalente a move(y,x); printw(cose);
 * 
 * WINDOW:
 * 
 * newwin(h, w, y, x)                          crea nuova finestra
 * refresh()                                  la fa effettivamente comparire su schermo, tipo
 * box(pwin, tb, lr)                          disegna una box nella pwin con i caratteri tb e lr per top-bottom e left-right
 * wborder(pwin, l, r, t, b, tl, tr, bl, br)  come box, ma permette di stabilire tutti i lati e tutti i corners
 * 
 * wprintw, mvwprintw
 * 
 * wrefresh(pwin)       aggiorna il contenuto della pwin
 * 
 * USEFUL STUFF:
 * 
 * cbreak()             assicura che venga letto Ctrl+C come comando di uscita
 * raw()                prende raw input (Ctrl+C viene letto coma carattere)
 * noecho()             non mostra i caratteri inseriti da tastiera
 * 
*/