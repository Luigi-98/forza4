#include <stdio.h>
#include <stdlib.h>

#include "interfaccia.h"
#include "interfaccia.hidden.h"
#include "../../include/my_defines.h"

#define MIN_W_MENU_WIN 10

my_int interfaccia_set_title(pvoid pinterfaccia_, pchar title)
{
  return 0;
}

pvoid interfaccia_init(pchar title)
{
  p_interfaccia pinterfaccia = (p_interfaccia) malloc(sizeof(_interfaccia));

  if (pinterfaccia==NULL)
  {
    return NULL;
  }

  return pinterfaccia;
}

void interfaccia_free(pvoid pinterfaccia_)
{
  free(pinterfaccia_);
}

my_int interfaccia_show_message(pvoid pinterfaccia_, pchar message)
{
  printf("%s\n", message);

  return 1;
}

void interfaccia_resize_pwin(p_interfaccia pinterfaccia, my_int w, my_int h)
{
  return;
}

my_int interfaccia_hide_message(pvoid pinterfaccia_)
{
  return 0;
}

my_int interfaccia_print_scacchiera(pvoid pinterfaccia_, pvoid pforza4_)
{
  if(pforza4_==NULL) return 0;

  char contenuto_celle[] = {'_', 'X', 'O', 'T'};
  my_int riga, colonna, i,
    righe = forza4_getH(pforza4_),
    colonne = forza4_getW(pforza4_);
  
  /** crea lato superiore del riquadro **/
  for(i=0; i<colonne; i++)
   {
    printf("---");
   }
  printf("----\n");

  /** crea parte centrale del riquadro **/
  for (riga=0; riga<righe; riga++)
  {
    printf("| ");
    for (colonna=0; colonna<colonne; colonna++)
    {
      printf(" %c ", contenuto_celle[forza4_get_cell(pforza4_, riga, colonna)]);
    }
    printf(" |\n");
  }

  printf("| ");
  for (colonna=0; colonna<colonne; colonna++)
  {
    printf(" %d ", colonna+1);
  }
  printf(" |\n");

  /** crea lato inferiore del riquadro **/
  for(i=0; i<colonne; i++)
   {
    printf("---");
   }
  printf("----\n");

  return 1;
}

my_int interfaccia_scanf_mossa(pvoid pinterfaccia_, pvoid pforza4_, my_int giocatore)
{
    char str[50];
    my_int mossa;

    printf("Mossa giocatore %d (q termina la partita): ", giocatore);
    scanf("%s", str);

    if (*str=='q')
      return -1;

    mossa=atoi(str);

    if ((0<mossa)&&(forza4_getW(pforza4_)>=mossa))
    {
      return mossa;
    }

    return 0;
}

void interfaccia_wait()
{
  return;
}

my_int interfaccia_fastprintln_bigmessage(pvoid pinterfaccia_, pchar txt)
{
  return printf("%s\n", txt);
}

my_int interfaccia_hide_bigmessage(pvoid pinterfaccia_)
{
  return 0;
}

my_int interfaccia_show_percentage(pvoid pinterfaccia_, my_int percentage)
{
  return printf("%d%% - ", percentage);
}

my_int interfaccia_textmenu(pwindow pwin, ppchar pitems, my_int n_items, my_int x, my_int y)
{
  my_int counter, item;

  printf("Menu:\n");
  for (counter=0; counter<n_items; counter++)
  {
    printf("%d) %s\n", counter, pitems[counter]);
  }

  printf("Inserisci numero: ");
  scanf("%d", &item);

  return item;
}

my_int interfaccia_scanf_int_centered(pvoid pnull, pchar txt, my_int width)
{
  my_int res;

  printf("%s", txt);
  scanf("%d", &res);

  return res;
}