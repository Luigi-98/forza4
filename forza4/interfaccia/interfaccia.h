#ifndef _INTERFACCIA_H
#define _INTERFACCIA_H

#include "../../include/my_defines.h"
#include "forza4/forza4.h"

pvoid interfaccia_init(pchar title);
void interfaccia_free(pvoid pinterfaccia_);

my_int interfaccia_set_title(pvoid pinterfaccia_, pchar title);

/* stampa la scacchiera
 *
 * pinterfaccia_:           istanza di interfaccia
 * pstato:                  istanza di stato_partita
 * 
 * returns      1 o 0 in caso di successo o fallimento
 * */
my_int interfaccia_print_scacchiera(pvoid pinterfaccia_, pvoid pstato);

/* chiede all'utente la mossa da eseguire; ritorna la scelta dell'utente
 *
 * pinterfaccia_:           istanza di interfaccia (l'interfaccia da cui chiedere la mossa)
 * pstato:                  istanza di stato_partita (lo stato della partita a cui aggiungere la mossa)
 * giocatore:               giocatore di cui chiedere la mossa (1 o 2)
 * */
my_int interfaccia_scanf_mossa(pvoid pinterfaccia_, pvoid pstato, my_int giocatore);

my_int interfaccia_show_message(pvoid pinterfaccia_, pchar message);
my_int interfaccia_hide_message(pvoid pinterfaccia_);

my_int interfaccia_show_bigmessage(pvoid pinterfaccia_);
my_int interfaccia_println_bigmessage(pvoid pinterfaccia_, pchar txt);
my_int interfaccia_fastprintln_bigmessage(pvoid pinterfaccia_, pchar txt);
my_int interfaccia_hide_bigmessage(pvoid pinterfaccia_);

void interfaccia_refresh(pvoid pinterfaccia_, my_int message);

my_int interfaccia_show_percentage(pvoid pinterfaccia_, my_int percentage);

// menu:   stampa il menu delle possibili cose che può fare l'utente, ovvero: allena, gioca con pc, gioca con umano, impostazioni, esci; aspetta che l'utente faccia una scelta in loop fino a quando non si esce dal gioco
my_int interfaccia_menu(pvoid pinterfaccia_, pvoid pforza4_);

void interfaccia_wait();

#endif