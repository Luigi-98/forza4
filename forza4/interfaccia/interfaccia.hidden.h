#ifndef _INTERFACCIA_HIDDEN_H
#define _INTERFACCIA_HIDDEN_H

#include "interfaccia.h"

#include <ncurses.h>

typedef WINDOW window;
typedef window* pwindow;

typedef struct __interfaccia
{
    pwindow pwin, pmessagewin, pbigmessagewin;
    pchar title;
    pvoid pbigmessagewincontentqueue;
    int bigmessagewin_h,bigmessagewin_w;
} _interfaccia;

typedef _interfaccia* p_interfaccia;

char contenuto_celle[] = {'_', 'X', 'O', 'T'};

#endif