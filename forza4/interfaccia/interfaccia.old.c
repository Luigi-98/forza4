#include <stdio.h>
#include <limits.h>

#include <time.h>
#include <stdlib.h>
#include <string.h>

#include <ncurses.h>

#include "interfaccia.h"
#include "interfaccia.hidden.h"
#include "my_defines.h"

#define MAX_SCANF_INT 15
#define MIN_W_BIG_WIN 10
#define MIN_H_BIG_WIN 10
#define MIN_W_MENU_WIN 10
#define WIDTH_PERCENTAGE_BAR 20
#define STD_PWIN_SIZE 10

#define SCANF_BIGJUMP 4

#define IN_RANGE(x, a, b) (((x)<=(b))&&((x)>=(a)))

#define MY_KEY_CBREAK 3
#define MY_KEY_BACKSPACE  127
#define MY_KEY_ESCAPE 27

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

char contenuto_celle[] = {'_', 'X', 'O', 'T'};

int interfaccia_set_title(pvoid pinterfaccia_, pchar title)
{
  return 0;
}

pvoid interfaccia_init(pchar title)
{
  p_interfaccia pinterfaccia = (p_interfaccia) malloc(sizeof(_interfaccia));

  if (pinterfaccia==NULL)
  {
    return NULL;
  }

  return pinterfaccia;
}

void interfaccia_free(pvoid pinterfaccia_)
{
  free(pinterfaccia_);
}

int interfaccia_show_message(pvoid pinterfaccia_, pchar message)
{
  printf("%s\n", message);

  return 1;
}

void interfaccia_resize_pwin(p_interfaccia pinterfaccia, int w, int h)
{
  return;
}

int interfaccia_hide_message(pvoid pinterfaccia_)
{
  return 0;
}

my_int interfaccia_print_scacchiera(pvoid pinterfaccia_, pvoid pforza4_)
{
  if(pforza4_==NULL) return 0;

  char contenuto_celle[] = {'_', 'X', 'O', 'T'};
  my_int riga, colonna, i,
    righe = forza4_getH(pforza4_),
    colonne = forza4_getW(pforza4_);
  
  /** crea lato superiore del riquadro **/
  for(i=0; i<colonne; i++)
   {
    printf("---");
   }
  printf("----\n");

  /** crea parte centrale del riquadro **/
  for (riga=0; riga<righe; riga++)
  {
    printf("| ");
    for (colonna=0; colonna<colonne; colonna++)
    {
      printf(" %c ", contenuto_celle[forza4_get_cell(pforza4_, riga, colonna)]);
    }
    printf(" |\n");
  }

  printf("| ");
  for (colonna=0; colonna<colonne; colonna++)
  {
    printf(" %d ", colonna+1);
  }
  printf(" |\n");

  /** crea lato inferiore del riquadro **/
  for(i=0; i<colonne; i++)
   {
    printf("---");
   }
  printf("----\n");

  return 1;
}

my_int interfaccia_scanf_mossa(pvoid pinterfaccia_, pvoid pforza4_, my_int giocatore)
{
    char str[50];
    int mossa;

    printf("Mossa giocatore %d (q termina la partita): ", giocatore);
    scanf("%s", str);

    if (*str=='q')
      return -1;

    mossa=atoi(str);

    if ((0<mossa)&&(forza4_getW(pforza4_)>=mossa))
    {
      return mossa;
    }

    return 0;
}

void interfaccia_wait()
{
  return;
}

int interfaccia_fastprintln_bigmessage(pvoid pinterfaccia_, pchar txt)
{
  return printf("%s\n", txt);
}

int interfaccia_hide_bigmessage(pvoid pinterfaccia_)
{
  return 0;
}

int interfaccia_show_percentage(pvoid pinterfaccia_, my_int percentage)
{
  return printf("%d%% - ", percentage);
}

int interfaccia_textmenu(pwindow pwin, ppchar pitems, my_int n_items, int x, int y)
{
  my_int counter, item;

  printf("Menu:\n");
  for (counter=0; counter<n_items; counter++)
  {
    printf("%d) %s\n", counter, pitems[counter]);
  }

  printf("Inserisci numero: ");
  scanf("%d", &item);

  return item;
}

int interfaccia_scanf_int_centered(pvoid pnull, pchar txt, int width)
{
  int res;

  printf("%s", txt);
  scanf("%d", &res);

  return res;
}

my_int interfaccia_settings_menu(pvoid pinterfaccia_, pvoid pforza4_)
{
  p_interfaccia pinterfaccia = (p_interfaccia)pinterfaccia_;

  pchar menuitems[]={ "Dimensione della scacchiera",
                      "Indietro"};
  int n_items=2;
  int counter, val;
  int input0, input1;
  int x=3, y=2;
  
  start_menu:

  val=interfaccia_textmenu(pinterfaccia->pwin, menuitems, n_items, x, y);

  switch (val)
  {
    case 0:
      printf("L'impostazione attuale è %dx%d (inserire 0 per mantenere invariata)\n", forza4_getH(pforza4_), forza4_getW(pforza4_));
      input1 = interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Inserisci il numero di righe: ", 0);
      input0 = interfaccia_scanf_int_centered(pinterfaccia->pmessagewin, "Inserisci il numero di colonne: ", 0);
      forza4_set_size(pforza4_, input1, input0);
      break;
    case -1:
    case 1:
      if (forza4_impostazioni_save(pforza4_))
      {
        interfaccia_show_message(pinterfaccia_, "Impostazioni salvate!");
        interfaccia_wait();
        interfaccia_hide_message(pinterfaccia_);
      }
      else
      {
        interfaccia_show_message(pinterfaccia_, "Non sono riuscito a salvare le impostazioni!");
        interfaccia_wait();
        interfaccia_hide_message(pinterfaccia_);
      }
      return 1;
  }
  goto start_menu;
}

my_int interfaccia_menu(pvoid pinterfaccia_, pvoid pforza4_)
{
  p_interfaccia pinterfaccia = (p_interfaccia)pinterfaccia_;
  pchar menuitems[]={ "Gioca con umano",
                      "Gioca con PC",
                      "Allena PC",
                      "Impostazioni",
                      "Esci"};
  int n_items = 5;
  int val, counter;
  int x=3, y=2;
  int maxlen=MIN_W_MENU_WIN-4;

  for (counter=0; counter<n_items; counter++)
  {
    maxlen = MAX(maxlen, (int)strlen(menuitems[counter]));
  }

  start_menu:
  interfaccia_resize_pwin(pinterfaccia, maxlen+2*x, n_items+2*y);

  val=interfaccia_textmenu(NULL, menuitems, n_items, x, y);
  
  switch (val)
  {
    case 0:
      forza4_gioca_vs_umano(pforza4_, pinterfaccia_);
      break;
    case 1:
      forza4_gioca_vs_pc(pforza4_, pinterfaccia_);
      break;
    case 2:
      forza4_allena(pforza4_, pinterfaccia_, 10, 100, 2);
      break;
    case 3:
      interfaccia_settings_menu(pinterfaccia_, pforza4_);
      break;
    case -1:
    case 4:
      return EXIT_SUCCESS;
  }
  goto start_menu;
}