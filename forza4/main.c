#include<stdlib.h>
#include<stdio.h>

#include "interfaccia/interfaccia.h"
#include "interfaccia/forza4/forza4.h"

#include <ncurses.h>

int main(void);

int main(void)
{
  pvoid pinterfaccia = NULL;
  int exit_status=0;

  pinterfaccia = interfaccia_init("Forza4");
  if (pinterfaccia==NULL)
  {
    printf("Non abbastanza memoria!\n");
    return EXIT_FAILURE;
  }
  
  pvoid pforza4 = forza4_init();
  forza4_set_print(pforza4, interfaccia_print_scacchiera);
  forza4_set_scanf_mossa(pforza4, interfaccia_scanf_mossa);
  forza4_set_show_hide_message(pforza4, interfaccia_show_message, interfaccia_hide_message);
  forza4_set_wait(pforza4, interfaccia_wait);
  forza4_set_print_hide_big(pforza4, interfaccia_fastprintln_bigmessage, interfaccia_hide_bigmessage);
  forza4_set_print_hide_percentage(pforza4, interfaccia_show_percentage, interfaccia_hide_message);

  exit_status=interfaccia_menu(pinterfaccia, pforza4);

  forza4_free(pforza4);
  interfaccia_free(pinterfaccia);
  
  return exit_status;
}