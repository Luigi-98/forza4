// LETTURA
int main(void)
{
    pvoid pimp;

    typedef struct _mystruct
    {
        int b,a,c;
        pchar txt;
    } mystruct;

    mystruct str;

    pvoid plista_campi = lista_campi_init();

    lista_campi_add(plista_campi, 2, sizeof(int), 0); // b
    lista_campi_add(plista_campi, 1, sizeof(int), 0); // a
    lista_campi_add(plista_campi, 3, sizeof(int), 0); // c
    lista_campi_add(plista_campi, 4, sizeof(char)*12, 1); // txt

    pimp = impostazioni_init(plista_campi, "/tmp/prova.bin");
    printf("%d\n",impostazioni_load(pimp, &str));

    printf("%d, %d, %d, %s\n",str.a, str.b, str.c, str.txt);

    return EXIT_SUCCESS;
}

// SCRITTURA
int main(void)
{
    pvoid pimp;

    typedef struct _mystruct
    {
        int a,b,c;
        pchar txt;
    } mystruct;

    mystruct str = {5,6,7, "abc def    "};

    pvoid plista_campi = lista_campi_init();

    lista_campi_add(plista_campi, 1, sizeof(int), 0); // a
    lista_campi_add(plista_campi, 2, sizeof(int), 0); // b
    lista_campi_add(plista_campi, 3, sizeof(int), 0); // c
    lista_campi_add(plista_campi, 4, sizeof(char)*12, 1); // txt

    //lista_campi_printf(plista_campi);

    pimp = impostazioni_init(plista_campi, "/tmp/prova.bin");
    printf("%d\n",impostazioni_save(pimp, &str));

    return EXIT_SUCCESS;
}