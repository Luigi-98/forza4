#include "impostazioni.h"
#include "impostazioni.hidden.h"
#include "../debug/debug.h"
#include "../my_defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG 0

pvoid impostazioni_init(pvoid plista_campi_, pchar filename)
{
    p_impostazioni pimpostazioni = (p_impostazioni) malloc(sizeof(_impostazioni));
    int filename_len;

    if (pimpostazioni==NULL)
    {
        return NULL;
    }

    pimpostazioni->plista_campi_=plista_campi_;

    filename_len=strlen(filename)+1;
    pimpostazioni->filename=(pchar)malloc(filename_len);

    memcpy(pimpostazioni->filename, filename, filename_len);

    return (pvoid)pimpostazioni;
}

void impostazioni_free(pvoid pimpostazioni_)
{
    p_impostazioni pimpostazioni = (p_impostazioni) pimpostazioni_;

    lista_campi_free(pimpostazioni->plista_campi_);
    free(pimpostazioni->filename);
    free(pimpostazioni);
}

/*
 * file:
 * 
 * n_elements| id| content| id| content
*/
int impostazioni_load(pvoid pimpostazioni_, pvoid pstruct_)
{
    p_impostazioni pimpostazioni = (p_impostazioni) pimpostazioni_;
    FILE* pfile;
    int n_elements, status, counter;
    int id, size, position;
    pvoid pcontent;

    pfile = fopen(pimpostazioni->filename, "rb");
    if (pfile == NULL)
    {
        return 0;
    }

    status=fread(&n_elements, sizeof(int), 1, pfile);
    if (status!=1) goto error;

    for (counter=0; counter<n_elements; counter++)
    {
        status = fread(&id, sizeof(int), 1, pfile);
        if (status!=1) goto error;

        size = lista_campi_get_size(pimpostazioni->plista_campi_, id);

        pcontent = malloc(size);
        if (pcontent==NULL) goto error;

        status = fread(pcontent, size, 1, pfile);
        if (status!=1)
        {
            free(pcontent);
            goto error;
        }

        position = lista_campi_get_structposit(pimpostazioni->plista_campi_, id);
        
        //printf("id: %d, size: %d, counter: %d, pos: %d, by_pt: %d\n", id, size, counter, position, lista_campi_is_bypointer(pimpostazioni->plista_campi_, id));
        if (lista_campi_is_bypointer(pimpostazioni->plista_campi_, id))
        {
            //*((ppvoid)(pstruct_+position))=pcontent;)
            memcpy(*((ppvoid)(pstruct_+position)), pcontent, size);
            free(pcontent);
        }
        else
        {
            memcpy(pstruct_+position, pcontent, size);
            free(pcontent);
        }
    }

    fclose(pfile);

    return 1;

    error:
    fclose(pfile);
    return 0;
}

int impostazioni_save(pvoid pimpostazioni_, pvoid pstruct_)
{
    p_impostazioni pimpostazioni = (p_impostazioni) pimpostazioni_;
    FILE* pfile;
    int n_elements, status, counter;
    int id, size, position, by_pointer;
    pvoid pcontent;

    pfile = fopen(pimpostazioni->filename, "wb");
    if (pfile == NULL)
    {
        return 0;
    }

    n_elements=lista_campi_len(pimpostazioni->plista_campi_);

    status=fwrite(&n_elements, sizeof(int), 1, pfile);
    if (status!=1) goto error;

    for (counter=0; counter<n_elements; counter++)
    {
        id = lista_campi_get_item_id(pimpostazioni->plista_campi_, counter);
        position = lista_campi_get_structposit(pimpostazioni->plista_campi_, id);
        by_pointer = lista_campi_is_bypointer(pimpostazioni->plista_campi_, id);
        size = lista_campi_get_size(pimpostazioni->plista_campi_, id);

        #if DEBUG
        printf("id: %d\nposition: %d\nby_pointer: %d\nsize: %d\n\n",id, position, by_pointer, size);
        #endif

        pcontent = pstruct_+position;
        if (by_pointer)
        {
            pcontent = *(ppvoid)(pcontent);
            if (pcontent==NULL) continue;
        }
        
        status = fwrite(&id, sizeof(int), 1, pfile);
        if (status!=1) goto error;

        status = fwrite(pcontent, size, 1, pfile);
        if (status!=1) goto error;
    }

    fclose(pfile);

    return 1;

    error:
    fclose(pfile);
    return 0;
}

void impostazioni_set_filename(pvoid pimpostazioni_, pchar filename)
{
    p_impostazioni pimpostazioni = (p_impostazioni) pimpostazioni_;
    int filename_len = strlen(filename);
    
    free(pimpostazioni->filename);

    pimpostazioni->filename = (pchar)malloc(filename_len);

    memcpy(pimpostazioni->filename, filename, filename_len);
}