#ifndef _IMPOSTAZIONI_H
#define _IMPOSTAZIONI_H

#include "../my_defines.h"
#include "lista_campi/lista_campi.h"

/*
 * La libreria permette di gestire struct che contengano variabili di dimensione
 * statica. Le variabili sono trattate nell'ordine in cui sono contenute nella struct
 * e identificate per mezzo di un indice stabilito in inserimento
*/

pvoid impostazioni_init(pvoid plista_campi_, pchar filename);
void impostazioni_free(pvoid pimpostazioni_);

int impostazioni_load(pvoid pimpostazioni_, pvoid pstruct_);
int impostazioni_save(pvoid pimpostazioni_, pvoid pstruct_);

void impostazioni_set_filename(pvoid pimpostazioni_, pchar filename);

#endif