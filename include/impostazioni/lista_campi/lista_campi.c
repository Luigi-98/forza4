#include "lista_campi.h"
#include "lista_campi.hidden.h"

#include "../../list/list.h"
#include "../../my_defines.h"

#include <stdlib.h>
#include <stdio.h>

pvoid lista_campi_init()
{
    return list_init();
}

void lista_campi_free(pvoid plista_campi_)
{
    int n_campi = list_len(plista_campi_);
    int i;

    for (i=0; i<n_campi; i++)
    {
        free(list_get_first(plista_campi_));
        list_remove_first(plista_campi_);
    }

    list_free(plista_campi_);
}

void lista_campi_add_(pvoid plista_campi_, int id, int posizione_relativa, int size, int by_pointer)
{
    p_lista_campi__campo pelem = (p_lista_campi__campo) malloc(sizeof(_lista_campi__campo));

    pelem->id=id;
    pelem->size=size;
    pelem->by_pointer=by_pointer;
    pelem->posizione_nella_struct = posizione_relativa;
    
    list_append(plista_campi_, pelem);
}

void lista_campi_add(pvoid plista_campi_, int id, int size, int by_pointer)
{
    p_lista_campi__campo plast = (p_lista_campi__campo)list_get_last(plista_campi_);
    
    if (plast!=NULL)
    {
        lista_campi_add_(plista_campi_,
            id,
            plast->posizione_nella_struct + ((plast->by_pointer)?((int)sizeof(pvoid)):(plast->size)),
            size,
            by_pointer);
    }
    else
    {
        lista_campi_add_(plista_campi_, id, 0, size, by_pointer);
    }
    
}

void lista_campi_removelast(pvoid plista_campi_)
{
    free(list_get_item(plista_campi_, list_len(plista_campi_)-1));
    list_remove_last(plista_campi_);
}

// pinput sarà un pint che contiene l'id da cercare
// pelem sarà un plista_campi__campo
int check_id(pvoid pinput, pvoid pelem)
{
    return ((p_lista_campi__campo)pelem)->id==*((pint)pinput);
}

int lista_campi_get_size(pvoid plista_campi_, int id)
{
    p_lista_campi__campo pmy_campo = list_search_element_by_function(plista_campi_, check_id, (pvoid)&id);
    
    return pmy_campo->size;
}

int lista_campi_get_structposit(pvoid plista_campi_, int id)
{
    p_lista_campi__campo pmy_campo = list_search_element_by_function(plista_campi_, check_id, (pvoid)&id);
    
    return pmy_campo->posizione_nella_struct;
}

int lista_campi_is_bypointer(pvoid plista_campi_, int id)
{
    p_lista_campi__campo pmy_campo = list_search_element_by_function(plista_campi_, check_id, (pvoid)&id);
    
    return pmy_campo->by_pointer;
}

int lista_campi_len(pvoid plista_campi_)
{
    return list_len(plista_campi_);
}

int lista_campi_get_item_id(pvoid plista_campi_, int item)
{
    return ((p_lista_campi__campo)list_get_item(plista_campi_, item))->id;
}

int lista_campi_printf(pvoid plista_campi_)
{
    int n_elem = list_len(plista_campi_);
    int counter;
    p_lista_campi__campo pcampo;

    printf(" ID:\t SIZEOF:\t POS:\t BYPTR:\n====\t========\t=====\t=======\n");
    for (counter=0; counter<n_elem; counter++)
    {
        pcampo=list_get_item(plista_campi_, counter);

        printf(" %d \t %d     \t %d  \t %d     \n", pcampo->id, pcampo->size, pcampo->posizione_nella_struct, pcampo->by_pointer);
    }

    return 0;
}