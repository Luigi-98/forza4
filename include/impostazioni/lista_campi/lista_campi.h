#ifndef _LISTA_CAMPI_H
#define _LISTA_CAMPI_H

#include "../../my_defines.h"

/* Dato il puntatore a una struct precedentemente allocata e il nome di una sua variabile,
 *  trova l'indirizzo relativo della variabile rispetto all'inizio della struct
 * */
#define LISTA_CAMPI_GET_RELATIVE_STRUCT_POS(pstruct, varname)   (my_int)(((pchar)(&((pstruct)->varname)))-((pchar)(pstruct)))

/* Costruttore e distruttore della classe lista_campi */
pvoid lista_campi_init();
void lista_campi_free(pvoid plista_campi_);

/* Permettono di aggiungere una variabile alla lista:
 * 
 * plista_campi_        è un oggetto di tipo lista_campi
 * id                   è l'id identificativo della variabile
 * size                 è la dimensione in memoria della variabile
 * by_pointer (bool)    permette di stabilire se nella posizione indicata si trova la variabile o un puntatore a essa
 * 
 * posizione_relativa   normalmente calcolata come posizione_relativa+size dell'ultimo elemento inserito,
 *                      nel caso di una struct non è garantito che sia possibile calcolarla così
 * */
void lista_campi_add(pvoid plista_campi_, int id, int size, int by_pointer);
void lista_campi_add_(pvoid plista_campi_, int id, int posizione_relativa, int size, int by_pointer);

/* Varie funzioni relative alla lista:
 * 
 * plista_campi_        è un oggetto di tipo lista_campi
 * */
void lista_campi_removelast(pvoid plista_campi_);
int lista_campi_len(pvoid plista_campi_);
int lista_campi_printf(pvoid plista_campi_);

/* Varie funzioni che permettono di leggere la lista:
 * 
 * plista_campi_        è un oggetto di tipo lista_campi
 * id                   è l'id identificativo della variabile
 * item                 è il numero ordinale dell'elemento nella lista
 * 
 * get_size             ritorna la size dell'elemento identificato dall'id
 * get_structposit      ritorna la posizione relativa dell'elemento identificato dall'id
 * get_item_id          ritorna l'id a partire dall'item
 * is_bypointer         ritorna vero se la variabile è indicata da un puntatore
 *                      o falso se la variabile si trova nella posizione specificata
 * */
int lista_campi_get_size(pvoid plista_campi_, int id);
int lista_campi_get_structposit(pvoid plista_campi_, int id);
int lista_campi_get_item_id(pvoid plista_campi_, int item);
int lista_campi_is_bypointer(pvoid plista_campi_, int id);

#endif