#ifndef _LISTA_CAMPI_HIDDEN_H
#define _LISTA_CAMPI_HIDDEN_H

#include "lista_campi.h"


typedef struct __lista_campi__campo
{
    int id, size;
    int posizione_nella_struct;
    int by_pointer;
} _lista_campi__campo;

typedef _lista_campi__campo* p_lista_campi__campo;

#endif