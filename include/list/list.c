#include "list.h"
#include "list.hidden.h"
#include "../my_defines.h"

#include <stdlib.h>

pvoid list_init()
{
    p_list_head pres = (p_list_head)malloc(sizeof(_list_head));
    if (pres==NULL)
    {
        return pres;
    }

    pres->pfirst=NULL;
    pres->len=0;

    return (pvoid)pres;
}

void list_free(pvoid plist_)
{
    p_list_head plist_head = (p_list_head)plist_;

    p_list_element plist_element = plist_head->pfirst, pnext;

    while (plist_element!=NULL)
    {
        pnext = plist_element->pnext;

        free(plist_element);

        plist_element=pnext;
    }

    free(plist_);
}

int list_append(pvoid plist_, pvoid pelem_)
{
    p_list_head plist_head = (p_list_head)plist_;

    p_list_element plist_element = plist_head->pfirst, plast;

    p_list_element p_new_list_elem = (p_list_element) malloc(sizeof(_list_element));
    if (p_new_list_elem==NULL)
    {
        return 0;
    }

    p_new_list_elem->pnext=NULL;
    p_new_list_elem->pelement=pelem_;

    plist_head->len++;

    if (plist_element==NULL)
    {
        plist_head->pfirst=p_new_list_elem;
    }
    else
    {
        while (plist_element!=NULL)
        {
            plast=plist_element;
            plist_element=plist_element->pnext;
        }

        plast->pnext=p_new_list_elem;
    }
    
    return 1;
}

int list_remove_last(pvoid plist_)
{
    p_list_head plist_head = (p_list_head)plist_;

    p_list_element plist_element = plist_head->pfirst, psecondlast=NULL;

    if (plist_element==NULL)
    {
        return 0;
    }

    plist_head->len--;

    while (plist_element->pnext!=NULL)
    {
        psecondlast=plist_element;
        plist_element=plist_element->pnext;
    }

    if (psecondlast==NULL)
    {
        free(plist_element);
        plist_head->pfirst=NULL;
        return 1;
    }

    free(plist_element);
    psecondlast->pnext=NULL;

    return 1;
}

pvoid list_get_item(pvoid plist_, int id)
{
    p_list_head plist_head = (p_list_head)plist_;

    p_list_element plist_element = plist_head->pfirst;

    if (plist_element==NULL)
    {
        return NULL;
    }

    while (id)
    {
        plist_element=plist_element->pnext;
        
        if (plist_element==NULL)
        {
            return NULL;
        }

        id--;
    }

    return plist_element->pelement;
}

int list_len(pvoid plist_)
{
    return ((p_list_head)plist_)->len;
}

pvoid list_search_element_by_function(pvoid plist_, condition_function function, pvoid pinput_)
{
    p_list_head plist_head = (p_list_head)plist_;

    p_list_element plist_element = plist_head->pfirst;

    if (plist_element==NULL)
    {
        return NULL;
    }

    while (!function(plist_element->pelement, pinput_))
    {
        plist_element=plist_element->pnext;
        
        if (plist_element==NULL)
        {
            return NULL;
        }
    }

    return plist_element->pelement;
}

pvoid list_get_last(pvoid plist_)
{
    p_list_head plist_head = (p_list_head)plist_;

    p_list_element plist_element = plist_head->pfirst;

    if (plist_element==NULL)
    {
        return NULL;
    }

    while (plist_element->pnext!=NULL)
    {
        plist_element=plist_element->pnext;
    }

    return plist_element->pelement;
}

pvoid list_get_first(pvoid plist_)
{
    p_list_head plist_head = (p_list_head)plist_;

    p_list_element plist_element = plist_head->pfirst;

    return plist_element->pelement;
}

pvoid list_remove_first(pvoid plist_)
{
    p_list_head plist_head = (p_list_head)plist_;

    p_list_element plist_element = plist_head->pfirst;

    pvoid pelem;

    if (plist_element == NULL)
    {
        return NULL;
    }

    pelem = plist_element->pelement;

    plist_head->pfirst = plist_element->pnext;
    plist_head->len--;

    free(plist_element);

    return pelem;
}