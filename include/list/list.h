#ifndef _LIST_H
#define _LIST_H

#include "../my_defines.h"

typedef int(*condition_function)(pvoid pinput, pvoid pelement);

// dà null se non riuscita
pvoid list_init();
void list_free(pvoid plist_);

// dà vero se riuscita, falso se no
int list_append(pvoid plist_, pvoid pelem);
int list_remove_last(pvoid plist_);
pvoid list_remove_first(pvoid plist_);

pvoid list_get_item(pvoid plist_, int id);
pvoid list_get_last(pvoid plist_);
pvoid list_get_first(pvoid plist_);
int list_len(pvoid plist_);

/*
 * cerca il primo elemento per cui la funzione torna vero
 * 
 * la funzione prende in input pinput e ogni elemento della lista
*/
pvoid list_search_element_by_function(pvoid plist_, condition_function function, pvoid pinput_);

#endif