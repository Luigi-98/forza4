#ifndef _LIST_HIDDEN_H
#define _LIST_HIDDEN_H

#include "list.h"

typedef struct __list_element
{
    pvoid pelement;
    struct __list_element* pnext;
} _list_element;

typedef _list_element* p_list_element;

typedef struct __list_head
{
    p_list_element pfirst;
    int len;
} _list_head;

typedef _list_head* p_list_head;

#endif