#include "../my_defines.h"
#include "minimax.h"
#include "minimax.hidden.h"

#include <limits.h>
#include <stdlib.h>

#include <stdio.h>

/*
 * E' il minimax vero e proprio: ci serve per fare meno accessi a puntatori.
 * 
 * Impongo depth>=1 (perché già da start_minimax ho gestito il caso depth==0),
 * così a depth==1 confronto direttamente le euristiche dei figli
 * 
 * */
my_int minimax_internal_euristica(p_minimax pminimax, pvoid psituazione_, pvoid pmodello_euristica_, my_int depth, my_int current_depth, my_int prossimo_giocatore, my_int giocatore_partenza, pmy_int pmossa)
 {
    my_int mossa, mossa_best=MINIMAX_NO_MOVE_FOUND, punteggio_best, punteggio, vittoria, pedina_inserita;
    my_int n_mosse = pminimax->n_mosse;

    // siamo in fase max (o in fase min)?
    my_int usemax=(prossimo_giocatore==giocatore_partenza);
    
    punteggio_best=usemax?INT_MIN:INT_MAX;
    
    // genero le prossime possibili mosse
    for (mossa=0; mossa<n_mosse; mossa++)
    {
        // faccio la mossa
        pedina_inserita = pminimax->inserisci_pedina(psituazione_, prossimo_giocatore, mossa);
        
        // se non ho potuto fare questa mossa, passo alla successiva
        if (!pedina_inserita) continue;
        
        //tris_print(psituazione_);
        
        vittoria = pminimax->vittoria(psituazione_);
        
        // se il nodo è terminale perché c'è una vittoria o perché sono
        //  arrivato al fondo       ==> euristica
        //
        // se no
        //                          ==> max/min punteggi
        if (vittoria) // qualcuno ha vinto
        {
            punteggio = ((vittoria==giocatore_partenza)?
                pminimax->punteggio_vittoria:
                pminimax->punteggio_perdita) / current_depth;
        }
        else if (depth==1) // nodo terminale
        {
            punteggio = pminimax->euristica(pmodello_euristica_, psituazione_, giocatore_partenza);
        }
        else // ci sono figli
        {
            punteggio = minimax_internal_euristica(pminimax, psituazione_, pmodello_euristica_, depth-1, current_depth+1, prossimo_giocatore^3, giocatore_partenza, NULL);
        }
        
        // annullo la mossa
        pminimax->rimuovi_pedina(psituazione_, prossimo_giocatore, mossa);
        
        if (usemax?
                (punteggio>=punteggio_best):
                (punteggio<=punteggio_best)
            )
        {
            mossa_best=mossa;
            punteggio_best=punteggio;
        }
    }
    
    if (mossa_best==MINIMAX_NO_MOVE_FOUND) // non è stato possibile inserire pedine (nodo terminale)
    {
        punteggio_best = 0;//pminimax->euristica(pmodello_euristica_, psituazione_, giocatore_partenza);
    }
    
    if (pmossa!=NULL)
    {
        *pmossa=mossa_best;
    }
    
    return punteggio_best;
 }

my_int minimax_internal_aggiorna_euristica(p_minimax pminimax, pvoid psituazione_, pvoid pmodello_euristica_, my_int depth, my_int current_depth, my_int prossimo_giocatore, my_int giocatore_partenza, pmy_int pmossa, my_int euristica_iniziale)
 {
    my_int mossa, mossa_best=MINIMAX_NO_MOVE_FOUND, punteggio_best, punteggio, vittoria, nuova_euristica;
    my_int n_mosse = pminimax->n_mosse;
    
    // siamo in fase max (o in fase min)?
    my_int usemax=(prossimo_giocatore==giocatore_partenza);
    
    punteggio_best=usemax?INT_MIN:INT_MAX;
    
    // genero le prossime possibili mosse
    for (mossa=0; mossa<n_mosse; mossa++)
    {
        // faccio la mossa e aggiorno l'euristica
        nuova_euristica = pminimax->aggiorna_euristica(psituazione_, pmodello_euristica_, euristica_iniziale, giocatore_partenza, prossimo_giocatore, mossa);
        
        // se non ho potuto fare questa mossa, passo alla successiva
        if (nuova_euristica==pminimax->aggiorna_euristica_mossa_non_lecita) continue;
        
        //tris_print(psituazione_);
        
        vittoria = pminimax->vittoria(psituazione_);
        
        // se il nodo è terminale perché c'è una vittoria o perché sono
        //  arrivato al fondo       ==> euristica
        //
        // se no
        //                          ==> max/min punteggi
        if (vittoria) // qualcuno ha vinto
        {
            punteggio = ((vittoria==giocatore_partenza)?
                pminimax->punteggio_vittoria:
                pminimax->punteggio_perdita) / current_depth;
        }
        else if (depth==1) // nodo terminale
        {
            punteggio = nuova_euristica;
        }
        else // ci sono figli
        {
            punteggio = minimax_internal_aggiorna_euristica(pminimax, psituazione_, pmodello_euristica_, depth-1, current_depth+1, prossimo_giocatore^3, giocatore_partenza, NULL, nuova_euristica);
        }
        
        // annullo la mossa
        pminimax->rimuovi_pedina(psituazione_, prossimo_giocatore, mossa);
        
        if (usemax?
                (punteggio>=punteggio_best):
                (punteggio<=punteggio_best)
            )
        {
            mossa_best=mossa;
            punteggio_best=punteggio;
        }
    }
    
    if (mossa_best==MINIMAX_NO_MOVE_FOUND) // non è stato possibile inserire pedine (nodo terminale)
    {
        punteggio_best = 0;//euristica_iniziale;
    }
    
    if (pmossa!=NULL)
    {
        *pmossa=mossa_best;
    }
    
    return punteggio_best;
 }

my_int minimax_start(pvoid pminimax_, pvoid psituazione_, pvoid pmodello_euristica_, my_int depth, pmy_int pscore, my_int prossimo_giocatore)
{
    my_int vittoria;
    my_int mossa;
    my_int euristica_iniziale;
    my_int score;
    p_minimax pminimax=(p_minimax)pminimax_;
    
    if ((vittoria=(pminimax->vittoria(psituazione_))))
    {
        score=(vittoria==prossimo_giocatore)?
                pminimax->punteggio_vittoria:
                pminimax->punteggio_perdita;
        return MINIMAX_NO_MOVE_FOUND;
    }
    if ((!depth))
    {
        score=pminimax->euristica(pmodello_euristica_, psituazione_, prossimo_giocatore);
        return MINIMAX_NO_MOVE_FOUND;
    }
    if (pminimax->aggiorna_euristica==NULL)
    {
        score=minimax_internal_euristica(pminimax, psituazione_, pmodello_euristica_, depth, 1, prossimo_giocatore, prossimo_giocatore, &mossa);
    }
    else
    {
        euristica_iniziale = pminimax->euristica(pmodello_euristica_, psituazione_, prossimo_giocatore);
        score=minimax_internal_aggiorna_euristica(pminimax, psituazione_, pmodello_euristica_, depth, 1, prossimo_giocatore, prossimo_giocatore, &mossa, euristica_iniziale);
    }

    if (pscore != NULL)
        *pscore = score;
    
    return mossa;
}


pvoid minimax_init(p_inserisci_pedina_t inserisci_pedina, p_rimuovi_pedina_t rimuovi_pedina, p_vittoria_t vittoria, my_int n_mosse, my_int punteggio_vittoria, my_int punteggio_perdita)
{
    _minimax _tmpminimax = { .euristica = NULL,
                                .aggiorna_euristica = NULL,
                                .inserisci_pedina = inserisci_pedina,
                                .rimuovi_pedina = rimuovi_pedina,
                                .vittoria = vittoria,
                                .n_mosse = n_mosse,
                                .punteggio_vittoria = punteggio_vittoria,
                                .punteggio_perdita = punteggio_perdita };
    p_minimax pminimax = (p_minimax) malloc(sizeof(_minimax));
    if (pminimax==NULL) return pminimax;
    
    *pminimax=_tmpminimax;
    
    return (pvoid)pminimax;
}

void minimax_free(pvoid pminimax_)
{
    free(pminimax_);
}

void minimax_set_euristica(pvoid pminimax_, p_euristica_t euristica)
{
    p_minimax pminimax = (p_minimax) pminimax_;

    pminimax->euristica = euristica;
}

void minimax_set_aggiorna_euristica(pvoid pminimax_, p_aggiorna_euristica_t aggiorna_euristica, my_int aggiorna_euristica_mossa_non_lecita)
{
    p_minimax pminimax = (p_minimax) pminimax_;

    pminimax->aggiorna_euristica = aggiorna_euristica;
    pminimax->aggiorna_euristica_mossa_non_lecita = aggiorna_euristica_mossa_non_lecita;
}