#ifndef _MINIMAX_H
#define _MINIMAX_H

#include "../my_defines.h"
#include "tipi_puntatori_funzioni.h"

#define MINIMAX_NO_MOVE_FOUND -1

/*
 * Come utilizzare la classe Minimax.
 * =================================
 * 
 * Step obbligatori:
 * 1. Costruire l'oggetto con minimax_init(...)
 * 2. Impostare la funzione euristica
 * 3. Alla fine distruggere l'oggetto
 * 
 * La scelta dell'utilizzo di **euristica o aggiorna_euristica** è
 * esclusivamente dovuta all'inserimento o meno di aggiorna_euristica.
 * Se viene inserita viene utilizzata anche quella, se no solo euristica.
 * 
 * La funzione euristica è **obbligatoria**.
 * 
 * Per la **stima della migliore mossa** va utilizzata la funzione minimax_start(...).
 * */


/*
 * Stima la migliore mossa da fare sulla base dell'algoritmo minimax e
 *            della situazione attuale.
 * 
 * pminimax_:        istanza dell'oggetto minimax
 * psituazione_:     puntatore alla struct che contiene le informazioni sulla
 *                          situazione a partire dalla quale stimare la migliore mossa
 * depth:            profondità alla quale deve fermarsi l'algoritmo mimimax
 * pscore:           puntatore alla variabile nella quale salvare il punteggio
 *                          della mossa migliore da fare
 * 
 * returns           la mossa consigliata
 *                   MINIMAX_NO_MOVE_FOUND se nessuna mossa è stata lecita
*/
my_int minimax_start(pvoid pminimax_, pvoid psituazione_, pvoid pmodello_euristica_, my_int depth, pmy_int pscore, my_int prossimo_giocatore);

/*
 * Costruttore e distruttore della classe Minimax.
 * 
 * pminimax_:             istanza dell'oggetto minimax
 * 
 * imposta le funzioni (descritte in tipi_puntatori_funzioni.h):
 *  * inserisci_pedina
 *  * rimuovi_pedina
 *  * vittoria
 * 
 * e imposta i valori di:
 *  * n_mosse               (il numero di mosse possibili nel gioco)
 *  * punteggio_vittoria    (il punteggio euristico associato alla vittoria)
 *  * punteggio_perdita     (il punteggio euristico associato alla perdita)
*/
pvoid minimax_init(p_inserisci_pedina_t inserisci_pedina, p_rimuovi_pedina_t rimuovi_pedina, p_vittoria_t vittoria, my_int n_mosse, my_int punteggio_vittoria, my_int punteggio_perdita);
void minimax_free(pvoid pminimax_);

/*
 * Imposta la funzione euristica o aggiorna_euristica.
 * */
void minimax_set_euristica(pvoid pminimax_, p_euristica_t euristica);
void minimax_set_aggiorna_euristica(pvoid pminimax_, p_aggiorna_euristica_t aggiorna_euristica, my_int aggiorna_euristica_mossa_non_lecita);


#endif
