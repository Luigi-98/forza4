#ifndef _MINIMAX_TIPI_PUNTATORI_FUNZIONI_H
#define _MINIMAX_TIPI_PUNTATORI_FUNZIONI_H

#include "../my_defines.h"

/* Calcola l'euristica rispetto a uno stato della partita e a un giocatore
 * a favore del quale verrà calcolata la bontà dello stato della partita.
 * 
 * pmodello_euristica_          parametri eventuali da passare alla funzione
 * pstato_partita_              lo stato della partita
 * giocatore                    il giocatore in questione
 * 
 * returns      l'euristica
 * */
typedef my_int (*p_euristica_t)(pvoid pmodello_euristica_, pvoid pstato_partita_, my_int giocatore);

/*
 * Fa la mossa in questione e calcola la nuova euristica partendo dall'euristica vecchia
 * (utile nel caso di euristiche dipendenti da quantità locali)
 * 
 * pstato_partita_          lo stato della partita
 * pmodello_                la variabile che contiene eventuali parametri della funzione aggiorna euristica
 * vecchia_euristica        l'euristica prima dell'aggiunta della pedina
 * giocatore                il giocatore che deve trarre vantaggio dalla mossa
 * mossa                    la mossa da aggiungere
 * 
 * returns:          la nuova euristica
 *                   un valore specificato in set_aggiorna_euristica nel caso di mossa non lecita
**/
typedef my_int (*p_aggiorna_euristica_t)(pvoid pstato_partita_, pvoid pmodello_, my_int vecchia_euristica, my_uint giocatore_partenza, my_uint giocatore, my_uint mossa);

/* Verifica se ci sono state vittorie e torna:
 *  - 0 nel caso di nessuna vittoria;
 *  - il giocatore che ha vinto altrimenti.
 * */
typedef my_int (*p_vittoria_t)(pvoid pstato_partita_);

/* Compie/annulla la mossa specificata a nome del giocatore specificato
 *
 * returns      vero se la pedina è stata inserita
 *              falso se la mossa era illegale
 * */
typedef my_int (*p_inserisci_pedina_t)(pvoid pstato, my_uint giocatore, my_uint mossa);
typedef void (*p_rimuovi_pedina_t)(pvoid pstato, my_uint giocatore, my_uint mossa);

#endif