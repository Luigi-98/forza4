#ifndef _MY_DEFINES_GENERAL
#define _MY_DEFINES_GENERAL

typedef void*          pvoid;
typedef pvoid*         ppvoid;

typedef int*           pint;
typedef char*		   pchar;
typedef pchar*		   ppchar;

typedef int            my_int;
typedef my_int*        pmy_int;

typedef unsigned int   my_uint;
typedef my_uint*       pmy_uint;

typedef float          my_float;
typedef my_float*      pmy_float;

typedef double         my_double;
typedef my_double*     pmy_double;

#endif