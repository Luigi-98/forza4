#include "queue.h"

#include "../my_defines.h"

#include "../list/list.h"

pvoid queue_init()
{
    pvoid plist_ = list_init();

    return plist_;
}

void queue_free(pvoid pqueue_)
{
    list_free(pqueue_);
}

my_int queue_add(pvoid pqueue_, pvoid pelem_)
{
    return list_append(pqueue_, pelem_);
}

pvoid queue_get(pvoid pqueue_)
{
    return list_remove_first(pqueue_);
}

pvoid queue_get_by_id(pvoid pqueue_, int id)
{
    return list_get_item(pqueue_, id);
}

my_int queue_len(pvoid pqueue_)
{
    return list_len(pqueue_);
}