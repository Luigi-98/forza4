#ifndef _QUEUE_H
#define _QUEUE_H

#include "../my_defines.h"


pvoid queue_init();
void queue_free(pvoid pqueue_);

my_int queue_add(pvoid pqueue_, pvoid pelem_);
pvoid queue_get(pvoid pqueue_);
pvoid queue_get_by_id(pvoid pqueue_, int id);
my_int queue_len(pvoid pqueue_);

#endif