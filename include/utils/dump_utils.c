#include<stdio.h>
#include<limits.h>

#include "../my_defines.h"

void print_char(unsigned char n)
{
	unsigned char mask=1u<<(CHAR_BIT-1);
	
	for (; mask; mask=mask>>1)
	{
		printf("%d", (n&mask)?1:0);
	}
}

void print_dump(pvoid pt, int size)
{
	pchar pid, pend;
	
	for (pid=(pchar)pt, pend=pid+size; pid<pend; pid++)
	{
		print_char(*pid);
		printf(" ");
	}
	
	printf("\n");
}