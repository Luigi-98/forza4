#ifndef _DUMP_UTILS_H
#define _DUMP_UTILS_H

#include "../my_defines.h"

void print_char(unsigned char n);

void print_dump(pvoid pt, int size);

#endif