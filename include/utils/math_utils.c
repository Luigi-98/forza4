#include "math_utils.h"
#include "../my_defines.h"

my_uint log_2(my_int val)
{
	my_int res;
	
	for (res=0; (val=(val>>1)); res++);
	
	return res;
}