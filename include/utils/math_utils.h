#ifndef _MATH_UTILS_H
#define _MATH_UTILS_H

#include "../my_defines.h"

#define IN_RANGE(x, a, b) (((x)<=(b))&&((x)>=(a)))

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

my_uint log_2(my_int val);

#endif