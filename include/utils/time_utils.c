#include <time.h>
#include "time_utils.h"

void time_delay(int seconds)
{
  time_t time0 = time(NULL);

  while (time(NULL)-time0<seconds);
}