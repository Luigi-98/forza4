SHELL=/bin/sh

DEBUGOPTIONS = #-pg -Wall -Wextra

CC=gcc
CFLAGS = $(DEBUGOPTIONS) -std=c99 $(LNCURSES) -O3 -Wno-unused-parameter #-m64
LDFLAGS = -r -arch x86_64 #-Ur

INTERFACCIA_OLD = 
LNCURSES = -lncurses

OUTFOLDER=.
MAINFOLDER=forza4
INTERFACCIA=$(MAINFOLDER)/interfaccia
FORZA4=$(INTERFACCIA)/forza4
STATO_PARTITA=$(FORZA4)/stato_partita

INCLUDES=$(OUTFOLDER)/include
IMPOSTAZIONI=$(INCLUDES)/impostazioni
LISTA_CAMPI=$(IMPOSTAZIONI)/lista_campi
LIST=$(INCLUDES)/list
QUEUE=$(INCLUDES)/queue
MINIMAX=$(INCLUDES)/minimax
UTILS=$(INCLUDES)/utils

$(info )
$(info **** STARTING ... ****)
$(info )
$(info )

$(OUTFOLDER)/main: $(INTERFACCIA)/interfaccia.o $(MAINFOLDER)/main.c $(INCLUDES)/includes.o
	$(info )
	$(info **** Compiling main... ****)
	$(info )
	$(info )
	$(CC) $(MAINFOLDER)/main.c $(INCLUDES)/includes.o $(INTERFACCIA)/interfaccia.o $(FORZA4)/forza4.o $(STATO_PARTITA)/stato_partita.o -o $(OUTFOLDER)/main $(CFLAGS)

$(INTERFACCIA)/interfaccia.o: $(INTERFACCIA)/interfaccia.c $(FORZA4)/forza4.o
	$(info )
	$(info **** Compiling interfaccia... ****)
	$(info )
	$(info )
	$(CC) -c $(INTERFACCIA)/interfaccia.c -o $(INTERFACCIA)/interfaccia.o $(CFLAGS) $(INTERFACCIA_OLD)

$(FORZA4)/forza4.o: $(FORZA4)/forza4.c $(FORZA4)/forza4.h $(FORZA4)/forza4.hidden.h $(FORZA4)/tipi_puntatori_funzioni.h $(STATO_PARTITA)/stato_partita.o
	$(info )
	$(info **** Compiling forza4... ****)
	$(info )
	$(info )
	$(CC) -c $(FORZA4)/forza4.c -o $(FORZA4)/forza4.o $(CFLAGS)

$(STATO_PARTITA)/stato_partita.o: $(STATO_PARTITA)/stato_partita.c $(STATO_PARTITA)/stato_partita.h $(STATO_PARTITA)/stato_partita.hidden.h
	$(info )
	$(info **** Compiling stato_partita... ****)
	$(info )
	$(info )
	$(CC) -c $(STATO_PARTITA)/stato_partita.c -o $(STATO_PARTITA)/stato_partita.o $(CFLAGS)

$(MINIMAX)/minimax.o: $(MINIMAX)/minimax.c $(MINIMAX)/minimax.hidden.h $(MINIMAX)/minimax.h $(MINIMAX)/tipi_puntatori_funzioni.h
	$(info )
	$(info **** Compiling minimax... ****)
	$(info )
	$(info )
	$(CC) -c $(MINIMAX)/minimax.c -o $(MINIMAX)/minimax.o $(CFLAGS)

$(LIST)/list.o: $(LIST)/list.c $(LIST)/list.h $(LIST)/list.hidden.h
	$(info )
	$(info **** Compiling list... ****)
	$(info )
	$(info )
	$(CC) -c $(LIST)/list.c -o $(LIST)/list.o $(CFLAGS)

$(QUEUE)/queue.o: $(QUEUE)/queue.c $(QUEUE)/queue.h $(LIST)/list.o
	$(info )
	$(info **** Compiling queue... ****)
	$(info )
	$(info )
	$(CC) -c $(QUEUE)/queue.c -o $(QUEUE)/queue.o $(CFLAGS)

$(LISTA_CAMPI)/lista_campi.o: $(LIST)/list.o $(LISTA_CAMPI)/lista_campi.c $(LISTA_CAMPI)/lista_campi.h $(LISTA_CAMPI)/lista_campi.hidden.h
	$(info )
	$(info **** Compiling lista_campi... ****)
	$(info )
	$(info )
	$(CC) -c $(LISTA_CAMPI)/lista_campi.c -o $(LISTA_CAMPI)/lista_campi.o $(CFLAGS)

$(IMPOSTAZIONI)/impostazioni.o: $(LISTA_CAMPI)/lista_campi.o $(IMPOSTAZIONI)/impostazioni.c $(IMPOSTAZIONI)/impostazioni.h $(IMPOSTAZIONI)/impostazioni.hidden.h
	$(info )
	$(info **** Compiling impostazioni... ****)
	$(info )
	$(info )
	$(CC) -c $(IMPOSTAZIONI)/impostazioni.c -o $(IMPOSTAZIONI)/impostazioni.o $(CFLAGS)

$(UTILS)/utils.o: $(UTILS)/math_utils.c $(UTILS)/math_utils.h $(UTILS)/dump_utils.c $(UTILS)/dump_utils.h $(UTILS)/time_utils.c $(UTILS)/time_utils.h 
	$(info )
	$(info **** Compiling utils... ****)
	$(info )
	$(info )
	$(CC) -c $(UTILS)/math_utils.c -o $(UTILS)/math_utils.o $(CFLAGS)
	$(CC) -c $(UTILS)/dump_utils.c -o $(UTILS)/dump_utils.o $(CFLAGS)
	$(CC) -c $(UTILS)/time_utils.c -o $(UTILS)/time_utils.o $(CFLAGS)
	ld $(LDFLAGS) $(UTILS)/math_utils.o $(UTILS)/dump_utils.o $(UTILS)/time_utils.o -o $(UTILS)/utils.o

$(INCLUDES)/includes.o: $(IMPOSTAZIONI)/impostazioni.o $(LIST)/list.o $(MINIMAX)/minimax.o $(QUEUE)/queue.o $(UTILS)/utils.o
	$(info )
	$(info **** Generating includes.o... ****)
	$(info )
	$(info )
	ld $(LDFLAGS) $(IMPOSTAZIONI)/impostazioni.o $(LISTA_CAMPI)/lista_campi.o $(LIST)/list.o $(MINIMAX)/minimax.o $(QUEUE)/queue.o $(UTILS)/utils.o -o $(INCLUDES)/includes.o


clear-objects:
	rm -f $(MINIMAX)/minimax.o
	rm -f $(IMPOSTAZIONI)/impostazioni.o
	rm -f $(LISTA_CAMPI)/lista_campi.o
	rm -f $(LIST)/list.o
	rm -f $(QUEUE)/queue.o
	rm -f $(INCLUDES)/includes.o

	rm -f $(FORZA4)/forza4.o
	rm -f $(STATO_PARTITA)/stato_partita.o
	rm -f $(INTERFACCIA)/interfaccia.o
	rm -f $(OUTFOLDER)/main

all: clear-objects $(OUTFOLDER)/main

interfaccia_old: INTERFACCIA_OLD= -D _INTERFACCIA_OLD=1
interfaccia_old: LNCURSES= 
interfaccia_old: all