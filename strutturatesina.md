Struttura tesina
================

Sostanzialmente seguire il percorso che abbiamo seguito noi.

 0. Abstract

 1. Introduzione
 >Poniamo il problema
 
 2. Related work
 >minimax, Montecarlo
 
 3. La nostra soluzione (algoritmo (abbiamo deciso di risolvere con minimax, costruire un'euristica con quel tipo di features e di quel tipo, sparare i coefficienti a caso))
    i. Algoritmo generale

 4. Implementazione
    i. Struttura globale del progetto e divisione in oggetti (aka le classi)
    i. Interfaccia
    i. Forza4
    i. Stato partita
        i. inserisci/rimuovi_pedina
        i. calcola features (con distinzione tra internal e quella vera)
        i. euristica globale
        i. aggiorna euristica
        i. check vittoria
    i. Minimax
    i. Struttura in classi. Per ogni classe:
        1. Cosa è capace di fare (panoramica delle funzioni)
        2. Strutture dati più importanti
        3. Prototipi

 5. Risultati sperimentali
 >tempi di allenamento prima di una convergenza definita in qualche modo

 6. Conclusioni, possibili miglioramenti
 >"è stato possibile realizzare un algoritmo che giochi a forza4, ma comunque c'è qualche problema nell'allenamento che si potrebbe migliorare così, in modo da permettere anche l'inserimento di più features"

 7. Bibliografia (?)
