% !TeX spellcheck = it_IT
\documentclass[a4paper,twocolumn,11pt]{IEEEtran}

\newcommand{\algoritmo}{Algoritmo }

\usepackage[english,italian]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{verbatim}
\usepackage{pdfpages}
\usepackage{caption}
\usepackage{svg}
\usepackage{geometry}
\usepackage[\algoritmo]{algorithm}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage{float}
\usepackage{braket}
\graphicspath{{./figure/}}

\title{Giocatore automatico di Forza4}
\author{L. Privitera, A. Santonocito}

\newcommand{\To}{\textbf{to} }
\newcommand{\hide}[1]{}

\newcommand{\debug}[1]{}

\begin{document}
\debug{
	\newpage
	\tableofcontents
	\newpage
}
\maketitle
\selectlanguage{english}
\begin{abstract}
	Abbiamo realizzato un giocatore automatico di Forza 4, con l'obiettivo di ottimizzare sia in velocità che in memoria, per piani di gioco di dimensione arbitraria.
	
	Il progetto è improntato principalmente sull'uso della memoria in forma bitwise e sulla progettazione di codice riutilizzabile, mantenendo comunque un buon compromesso con la velocità. L'approccio utilizzato si basa inoltre su tecniche di machine learning e l'interfaccia sulla libreria ncurses.
\end{abstract}
\selectlanguage{italian}

\section{Introduzione}
La progettazione di giocatori automatici si basa comunemente su algoritmi di stampo predittivo: provando a prevedere quali siano le successive mosse più probabili, si \textbf{sceglie di fare la mossa} che abbia la massima probabilità di portare alla vittoria. Nel caso specifico, abbiamo scelto di utilizzare l'approccio dell'algoritmo minimax.

Questo, però, si basa su una funzione che determini la \textbf{bontà euristica} di una determinata situazione di gioco. Nel caso di Forza4 è spesso auspicabile costruire delle determinate sequenze di pedine: terne di stesso colore seguite da una casella vuota o, magari, ambi promuovibili a terne. Generalizzando, valutiamo la bontà di una situazione di gioco a partire non solo dal numero di ambi e terne promuovibili, ma dal numero di quartetti di ogni tipo che troviamo sulla scacchiera. Questi numeri sono combinati linearmente per stimare la funzione euristica vera e propria.

Per la \textbf{stima dei coefficienti}, abbiamo utilizzato un approccio di learning: abbiamo generato casualmente delle n-uple di coefficienti per poi confrontarle tra di loro facendole giocare a coppie e scegliendo quella che vince più partite.

Abbiamo scelto, inoltre, di costruire un'\textbf{interfaccia} basata sulla libreria ncurses che quindi permette, seppur sempre da terminale, un approccio più intuitivo all'applicazione. Per non perdere di generalità e compatibilità tra i sistemi operativi, comunque, abbiamo scelto di separare delle funzioni ad alto livello, indipendenti dall'interfaccia, da funzioni a basso livello specifiche per l'interfaccia utilizzata. Per dimostrare la generalità di questo metodo, le funzioni a basso livello sono state scritte anche in una seconda versione basata su stdin ed stdout.

L'\textbf{applicazione} permette di giocare una partita sia contro un'altra persona che contro il computer stesso. Permette inoltre di avviare nuovi cicli di allenamento che permettano di migliorare ulteriormente il giocatore automatico.

\section{La nostra soluzione}
\subsection{Giocare una partita}
Una partita è trattata come una sequenza di mosse eseguite in alternanza dai due giocatori. Ad ogni mossa si controlla se uno dei due giocatori abbia vinto, quindi si decide se continuare la partita o attendere la mossa successiva (vedi \algoritmo \ref{pseudocodepartita}).

\begin{algorithm}
\caption{Partita}
\label{pseudocodepartita}
\begin{algorithmic}
\State $giocatore \gets \Call{rand}{0,1}$
\State \Call{azzera}{stato\ partita}
\Repeat
	\State $stato\ partita \gets \Call{inserisci mossa}{\null}$
\Until{\Call{parità}{\null} or \Call{vittoria}{\null}}
\end{algorithmic}
\end{algorithm}

L'inserimento della mossa successiva può avvenire mediante funzioni di interfaccia nel caso in cui il turno sia dell'umano o mediante chiamata alla funzione minimax nel caso in cui sia il turno della macchina. La struttura è la stessa in tutti e tre i casi di gioco: quello tra due giocatori umani, quello contro il computer e quello di allenamento computer contro computer.

\subsection{Scelta automatica della mossa}
Alla base di questa scelta sta la valutazione del \textbf{vantaggio} o dello \textbf{svantaggio} provocato da una certa situazione di gioco. Una situazione è \textit{vantaggiosa} se permette al computer di vincere facilmente, mentre è \textit{svantaggiosa} se invece lo permette all'avversario. Viene quindi valutato un indice di vantaggio sulla cui base viene scelta la mossa.

Facciamo un esempio nel caso di un gioco più semplice come il tris. In figura \ref{trisminimax} vediamo una situazione in cui in linea teorica il giocatore cerchio potrebbe compiere tutte le mosse rappresentate in tratteggiato, ma in tutti gli stati che raggiunge l'avversario vince in una sola mossa, tranne in quella in cui gli viene bloccata la terna vincente. In altri termini, tutte le situazioni di gioco sono svantaggiose, tranne una. In tal caso, la scelta che ci si aspetta da un algoritmo è proprio quella, per minimizzare dunque la probabilità di vittoria dell'avversario.

\begin{figure}[h]
	\centering
	\includegraphics[clip=true,width=0.8\linewidth]{trisminimax.pdf}
	\caption{L'unica mossa che non permette all'avversario di vincere facilmente è quella in alto.\label{trisminimax}}
\end{figure}

Idealmente si potrebbero tentare tutte le possibili combinazioni di mosse che portino alla fine della partita e verificare la probabilità di vittoria in funzione della prossima mossa, ma questo porta a due problemi:
\begin{enumerate}
	\item la probabilità che l'avversario faccia una mossa piuttosto che un'altra non è uguale né facile da stimare, perché dipende dalla sua convenienza e dalla sua esperienza di gioco;
	\item per valutare tutte queste combinazioni di mosse serve un algoritmo di classe NP rispetto alla dimensione della scacchiera.
\end{enumerate}
Per ovviare a questi problemi, si usa semplificare il problema aggiungendo delle ipotesi, magari limitando il numero di mosse predette.

\subsection{Minimax}
Nel nostro caso utilizziamo minimax in tutti i casi in cui il computer debba scegliere una mossa. Le limitazioni che impone sono le seguenti:
\begin{enumerate}
	\item la probabilità delle varie mosse viene discretizzata ai valori $0$ e $1$ ipotizzando che l'avversario sia un giocatore ideale che fa sempre la migliore mossa possibile e quindi la più svantaggiosa per il computer: quella mossa avrà probabilità $1$, mentre tutte le altre $0$;
	\item si limita il numero di mosse predette impostando una profondità massima: vengono tentate le successive mosse fino a che non si raggiunge quella profondità o finisce la partita. In tal caso, si valuta euristicamente la bontà della situazione di gioco mediante l'assegnazione di un punteggio.
\end{enumerate}

\begin{figure}[h]
\centering
\includegraphics[clip=true,width=1\linewidth]{Minimax.pdf}
\caption{Esempio di algoritmo minimax sull'albero delle mosse\label{minimaxtree}}
\end{figure}

Il punteggio così assegnato sostituisce, pertanto, la probabilità di vittoria e viene matematizzato nella maniera seguente (vedi figura \ref{minimaxtree}):
\begin{itemize}
	\item il \textbf{punteggio euristico} nel caso di una foglia dell'albero delle mosse, il cui obiettivo è quello di valutare la bontà della situazione di gioco per un certo giocatore;
	\item le \textbf{funzioni minimo e massimo} nel caso dei rami, il cui obiettivo è quello di scegliere sempre la mossa più probabile per l'avversario o la migliore per il computer.
\end{itemize}

Il punteggio euristico viene assegnato in tre casi diversi:
\begin{itemize}
	\item nel caso in cui si raggiunga la \textbf{profondità massima}, allora si valuta mediante un'apposita funzione di euristica la bontà di ogni situazione di gioco;
	\item nel caso in cui la partita finisce perché \textbf{uno dei due giocatori vince}, allora si assegna un punteggio che rappresenti la vittoria o la perdita;
	\item se la partita finisce in \textbf{parità}, allora si assegna un altro punteggio specifico.
\end{itemize}

Abbiamo scelto di assegnare il massimo e il minimo valore possibile per la vittoria e la perdita, mentre lo 0 in situazione di parità. La funzione di euristica, invece, sarà trattata in dettaglio in (\ref{algeuristica}).

\begin{algorithm}
	\caption{Minimax (giocatore=1)\label{minimaxalg}}
	\begin{algorithmic}
		\Function{minimax}{stato\ partita, profondità, prossimo\ giocatore}
		\If {scacchiera\ piena}
		\State \Return (no\ mossa, 0) % euristica
		\EndIf
		
		\ForAll{mosse possibili}
		\State \Call{inserisci\ mossa}{\null}
		\If {\Call{vittoria}{\null}}
		\If {giocatore 1}
		\State \Return vittoria/prof. attuale
		\Else
		\State \Return perdita/prof. attuale
		\EndIf
		\EndIf
		\If {profondità==1}
		\State \Call{calcola\ euristica}{\null}
		\Else
		\State \Call{minimax}{profondità-1}
		\EndIf
		\State \Call{annulla\ mossa}{\null}
		\EndFor
		\If {giocatore 1}
		\State \Return \Call{max}{euristiche}
		\Else
		\State \Return \Call{min}{euristiche}
		\EndIf
		\EndFunction
	\end{algorithmic}
\end{algorithm}

La complessità worst-case di questo algoritmo è, pertanto, $O({(n_\text{colonne})}^{\text{profondità}})$, polinomiale rispetto alla dimensione della scacchiera.

\subsection{Funzione di euristica}\label{algeuristica}
Per calcolare la funzione di euristica, ci basiamo sulle proprietà locali della scacchiera. Per ognuna delle $4$ direzioni, consideriamo tutti i possibili quartetti, ovvero tutte le possibili sequenze di $4$ celle consecutive (come in figura \ref{esempioquartetto}). Di ognuna di queste, contiamo il numero di volte in cui compare nella scacchiera (\algoritmo \ref{calcolofeatures})

\begin{figure}
	\centering
	\includegraphics[clip=true,width=0.8\linewidth]{schemacalcolofeatures.pdf}
	\caption{Esempio di quartetto $(\; - \, X \, O \, - \;)$\label{esempioquartetto}}
\end{figure}

Contando i vari quartetti otteniamo le variabili $n_1, n_2, \ldots$. Una generica $n_i$ indica il numero di volte in cui compare nella scacchiera il quartetto $i$-esimo. Ad esempio, in figura \ref{esempioquartetto}, il quartetto verticale $(\; - \, - \, - \, X \;)$ compare $4$ volte, pertanto la relativa $n_i$ varrà $4$.

Una volta calcolati questi valori, otteniamo la funzione euristica semplicemente come loro combinazione lineare:
$$E = c_1n_1+c_2n_2+c_3n_3+\ldots$$

A questo punto il problema del calcolo della funzione euristica si sposta al problema di determinare i coefficienti $c_1, c_2, \ldots$ della combinazione lineare. Li otteniamo utilizzando un sistema di allenamento, in modo da permettere il riconoscimento di tutte le combinazioni non banali utili alla vittoria.

\begin{algorithm}
	\caption{Calcolo features\label{calcolofeatures}}
	\begin{algorithmic}
		\State $contatori\ quartetti \gets \{0,0,\ldots\}$
		\ForAll{direzioni}
		\ForAll{quartetti}
		\State \Call{incrementa}{contatore\ quartetto}
		\EndFor
		\EndFor
	\end{algorithmic}
\end{algorithm}

\subsection{Allenamento}\label{allenamentosection}
Abbiamo scelto di generare casualmente un certo numero di n-uple confrontandole tra di loro e cercandone una capace di trovare strategie di gioco anche senza la previsione esplicita delle mosse successive. Per fare ciò abbiamo basato il confronto su delle partite giocate a coppie: si suppone che la n-upla vincitrice del maggior numero di partite sia la migliore tra le due. Per evitare che la "bravura" della n-upla sia dovuta all'algoritmo minimax, però, abbiamo scelto di impostare una profondità bassa durante l'allenamento.

\begin{figure}
	\centering
	\includegraphics[clip=true,width=0.8\linewidth]{allenamento.pdf}
	\caption{Schema torneo di allenamento}
	\label{allenamento}
\end{figure}

Come mostrato in figura \ref{allenamento}, l'allenamento è impostato come un torneo in 2 fasi:
\begin{enumerate}
	\item durante ogni \textit{ciclo} vengono generate $N_b$ n-uple, dunque vengono confrontate tra di loro le prime 2, la migliore di queste si scontra poi con la terza e così si va avanti finché, a fine ciclo, non rimane una sola n-upla;
	\item i \textit{cicli} vengono ripetuti $N_a$ volte, generando $N_a$ n-uple. Le n-uple uscenti dai vari cicli vengono a loro volta confrontate secondo lo stesso schema, ovvero prendendo delle prime due solo la migliore, confrontandola poi con la terza, finché non rimarrà quell'unica n-upla che sarà il risultato dell'allenamento.
\end{enumerate}

Un importante vantaggio di questa strategia è quello di potere essere estensibile: partendo dalla n-upla finale di un allenamento, lo si potrà continuare in maniera formalmente indistinguibile da una singola sessione. Ciò è possibile completando altri cicli e applicando lo stesso procedimento utilizzando la vecchia n-upla finale al posto del risultato del primo ciclo.

\begin{algorithm}
\caption{Allenamento}
\begin{algorithmic}
\Function{allenamento}{\null}
	\State n-upla1 $\gets$ \Call{ciclo}{$N_b$}
	\State n-upla2 $\gets$ \Call{ciclo}{$N_b$}
	\For {i in 1 \To $N_a$}
		\State \Call{confronta}{n-upla1, n-upla2}
		\State n-upla perdente $\gets$ \Call{ciclo}{$N_b$}
	\EndFor
\EndFunction
\end{algorithmic}

\begin{algorithmic}
\Function{ciclo}{$N_b$}
	\State n-upla1 $\gets \Call{rand}{ }$
	\State n-upla2 $\gets \Call{rand}{ }$
	\For {i in 1 \To $N_b$}
		\State \Call{confronta}{n-upla1, n-upla2}
		\State n-upla perdente $\gets \Call{rand}{ }$
	\EndFor
	\State\Return ultima n-upla vincente
\EndFunction
\end{algorithmic}
\end{algorithm}

\section{Implementazione}
Il codice è scritto a oggetti in linguaggio C. La finalità che ci siamo posti nella divisione in classi è quella di fare in modo che la maggior parte del codice fosse riutilizzabile e di aumentarne la leggibilità.

\paragraph{Oggetti}
Implementativamente costruiamo un oggetto accostando delle funzioni a delle struct che contengono le variabili.

Trattiamo tutte le variabili come private, rendendone pubbliche alcune utilizzando delle funzioni di interfaccia con l'oggetto, che abbiamo distinto dalle altre utilizzando le parole chiave \textit{get} e \textit{set}.

La forma delle struct in questione viene nascosta all'esterno (rendendo quindi effettivamente private le variabili) utilizzando puntatori di tipo \textit{pvoid} e definendo la struct in un file a parte che non viene quindi incluso durante l'utilizzo della libreria (il file di estensione \textit{.hidden.h}). Essendo le struct invisibili all'utilizzatore, c'è bisogno di due funzioni che facciano da costruttore e distruttore, che sono rispettivamente indicate con \textbf{init} e \textbf{free}.

Il contenuto delle funzioni viene scritto in dei file \textit{.c} che vengono quindi compilati in dei file oggetto (\textit{.o}), che nascondono i contenuti di tutte le funzioni, a meno di non aggiungerne i prototipi. Per rendere visibili le funzioni pubbliche, ne aggiungiamo i prototipi in dei file \textit{.h} che quindi verranno inclusi.

\paragraph{Struttura generale}
\begin{figure}
	\centering
	\includegraphics[clip=true,width=0.9\linewidth]{forza4classi.pdf}
	\caption{Schema implementativo della libreria\label{schemaclassi}}
\end{figure}

Abbiamo scelto di dividere l'interfaccia dal gioco vero e proprio. Così è possibile sostituirla riscrivendo solo il relativo file. Un'interazione ad alto livello con le partite e gli allenamenti è garantita invece dalla libreria forza4. Questa si basa internamente sulle librerie stato partita e minimax che permettono la gestione della memoria e il calcolo della mossa successiva. Per uno schema generale, vedi figura \ref{schemaclassi}.

\subsection{Interfaccia}
Per permettere la generalizzazione, abbiamo separato all'interno della stessa libreria interfaccia il codice ad alto livello da quello a basso livello. È stato possibile, infatti, individuare alcune azioni generali che una qualunque interfaccia deve poter compiere. Ad ognuna di queste è stato associato un set di funzioni:
\begin{itemize}
	\item \textbf{menu}(): permette di scegliere cosa fare;
	\item \textbf{print\_scacchiera}(stato): stampa la scacchiera;
	\item \textbf{scanf\_scacchiera}(stato, giocatore): chiede di fare una mossa;
	\item \textbf{show\_message}(messaggio), \textbf{hide\_message}(): mostrano un messaggio di notifica;
	\item \textbf{println\_bigmessage}(testo), \textbf{show\_bigmessage}(), \textbf{hide\_bigmessage}(), \textbf{fastprintln\_bigmessage}(testo): mostrano un feedback testuale per le operazioni di allenamento.
\end{itemize}

Le funzioni a basso livello che stanno dietro queste sono contenute nei file \textit{.core.h}. Questo contiene tutte quelle funzioni che permettono di inizializzare l'interfaccia (ad esempio nel caso di una finestra), di creare un menu testuale, di visualizzare messaggi e, in generale, tutto ciò che dipende specificatamente dalla libreria utilizzata per costruire l'interfaccia. Per dimostrare l'efficacia del metodo, sono state scritti due diversi file \textit{.core.h}, uno che si basa sulle funzioni ANSI C come printf e scanf, uno che si basa sulla libreria ncurses.

\subsection{Minimax}
L'utilizzo della libreria minimax è vincolato dall'esistenza delle funzioni \textbf{inserisci\_pedina}, \textbf{rimuovi\_pedina}, \textbf{vittoria} ed \textbf{euristica}. È possibile, inoltre, aggiungere anche la funzione \textbf{aggiorna\_euristica}.

La funzione \textbf{minimax\_start}(situazione, parametri\_euristica, profondità, prossimo\_giocatore) sceglie automaticamente come procedere verificando se sia stata inserita la funzione \textit{aggiorna\_euristica}, in quanto questa permette di rendere più efficiente l'algoritmo.

L'implementazione non presenta particolarità rispetto all'algoritmo \ref{minimaxalg}. Nello specifico, si è utilizzato un punteggio intero, in quanto nella maggior parte delle architetture già quello permette di distinguere con buona libertà la bontà delle situazioni di gioco, senza rinunciare all'efficienza del tipo \textit{int}. La scelta dei punteggi di vittoria e perdita è stata quella naturale di \textit{INT\_MAX} e \textit{INT\_MIN}.

\subsection{Stato partita}
Questa classe gestisce tutte le interazioni con la scacchiera. Quest'ultima è salvata all'interno della struct stato\_partita (vedi appendice A), in tutte le 4 direzioni: verticale, orizzontale e le due diagonali; ovvero, ad esempio, nella scacchiera salvata privilegiando la direzione verticale, i dati saranno relativi alle celle della prima colonna, poi della seconda colonna, e così via. Analogamente per le altre 3 direzioni.

Il tipo di dato che abbiamo scelto per il salvataggio delle informazioni è l'\textbf{int}, perché è il più veloce che il processore è in grado di gestire.

Ogni cella della scacchiera sarà rappresentata da due bit, e per la precisione:
\begin{itemize}
	\item 00 se la cella è vuota;
	\item 01 se la cella è occupata da una pedina del giocatore 1;
	\item 10 se la cella è occupata da una pedina del giocatore 2.
\end{itemize}
In alternativa avremmo potuto dividere la scacchiera in 2 (una per ogni giocatore) con celle da 1 bit (con valore 0 per la cella vuota e 1 per la cella occupata); tuttavia in questo modo, per distinguere se una cella è effettivamente vuota o occupata da una pedina dell'altro giocatore, bisogna obbligatoriamente guardare l'altra scacchiera. Questa soluzione, in termini computazionali, è peggiore.

In base alle dimensioni della scacchiera, ogni riga, colonna o diagonale entrerà in un certo numero di \textbf{int}; nel caso in cui rimangano bit in eccesso, questi saranno lasciati a 0 e non verranno considerati.

Abbiamo scelto di salvare 4 copie della stessa scacchiera (ma letta in direzioni diverse) per facilitare il calcolo delle features e abbassarne il tempo di lettura e la complessità computazionale: infatti, in alternativa, si sarebbero dovuti fare notevoli salti in memoria e applicare numerose operazioni bitwise per ricostruire i quartetti di celle lungo le 4 direzioni. Ovviamente, con questa scelta, si perde efficienza nell'inserimento e nella rimozione delle pedine (bisogna aggiornare 4 scacchiere ogni volta), ma ciò è ampiamente ricompensato dal notevole \emph{speed-up} che si ottiene per il calcolo delle features (che è l'operazione più onerosa in termini computazionali).

Di seguito un elenco delle principali funzioni della classe:
\begin{itemize}
	\item \textbf{inserisci\_pedina}(scacchiera, giocatore, mossa): inserisce una pedina del \emph{giocatore} 1 o 2 nella colonna indicata da \emph{mossa}.
	\item \textbf{rimuovi\_pedina}(scacchiera, giocatore, mossa): rimuove una pedina del \emph{giocatore} 1 o 2 nella colonna indicata da \emph{mossa}.
	\item \textbf{calcola\_features}(scacchiera, regione): calcola le features limitatamente a una \emph{regione} della \emph{scacchiera}.
	\item \textbf{euristica\_globale}(scacchiera, giocatore, coefficienti): calcola il valore dell'euristica (a favore del \emph{giocatore} 1 o 2) in una data situazione di gioco, sulla base dei \emph{coefficienti} dati in input e delle features calcolate mediante la funzione calcola\_features estesa all'intera scacchiera.
	\item \textbf{aggiorna\_euristica}(scacchiera, giocatore, vecchia\_euristica, mossa): aggiunge una pedina alla \emph{scacchiera} e calcola il valore della nuova euristica, basandosi solo sul cambiamento delle features avvenuto nell'intorno della pedina che è stata inserita.
	\item \textbf{check\_vittoria}(scacchiera): verifica se uno dei due giocatori ha vinto o se la partita è finita in parità.
	\item \textbf{allena}(scacchiera, numero\_partite\_per\_nupla, numero\_nuple\_da\_generare): il computer gioca contro sé stesso, generando n-uple casuali di coefficienti contro cui scontrarsi; se una n-upla perde, viene sostituita con un'altra generata casualmente.
\end{itemize}

Descriveremo adesso gli schemi di funzionamento delle principali funzioni della classe.

\subsubsection{Schema di funzionamento di inserisci\_perdina}
Supponiamo di voler inserire una pedina del giocatore 1 in una certa colonna della scacchiera. Per come sono stati organizzati i dati, bisogna aggiornare 4 scacchiere; per semplicità, vediamo solo l'algoritmo nel caso (ad esempio) della scacchiera letta in orizzontale (per le altre scacchiere il procedimento è analogo). Si noti che per le scacchiere lette in diagonale esiste una formula estremamente semplice per calcolare il numero della diagonale in cui inserire la pedina, conoscendo la riga e la colonna della stessa; per la precisione:
\[
\#diagonale = \#righe + colonna - riga - 1
\]
per la scacchiera diagonale "$\backslash$", e invece:
\[
\#diagonale = colonna + riga
\]
per la scacchiera diagonale "/".

In riferimento all'appendice B, vediamo uno schema delle operazioni che abbiamo scelto di svolgere per l'inserimento di una pedina:
\begin{enumerate}
	\item calcolo della posizione dell'\textbf{int} in cui si trova la cella da modificare all'interno della riga in questione (ad esempio, in figura, il quinto \textbf{int});
	\item calcolo della posizione all'interno dell'\textbf{int} in cui si trova la cella da modificare (in figura, la terza cella);
	\item shift opportuno della maschera per portare nella posizione giusta i 2 bit da inserire;
	\item XOR logico fra la maschera e l'\textbf{int} della scacchiera.
\end{enumerate}

\subsubsection{Schema di funzionamento di calcola\_features}
Per come è stata salvata la scacchiera in memoria, l'algoritmo per il calcolo delle features è lo stesso nelle 4 direzioni.

Una possibilità sarebbe potuta essere quella di generare una \emph{lookup table} con le codifiche binarie di tutti i quartetti ritenuti di interesse (ad esempio gli ambi o le terne promuovibili a quaterne) e poi fare delle operazioni bitwise con gli \textbf{int} della scacchiera per individuare ciascuna feature. Tuttavia, questo metodo richiederebbe di fare un gran numero di operazioni bitwise per ogni quaterna di celle, quindi abbiamo deciso di operare in un modo diverso: dato che ogni cella è rappresentata da 2 bit, ogni quaterna di celle è rappresentata da 8 bit, ovvero un numero decimale compreso fra 0 e 255. Quindi, basta isolare tutti i gruppi di 8 bit all'interno di ogni \textbf{int} e poi considerare il corrispondente numero decimale per individuare il tipo di quartetto; ad esempio, gli 8 bit 01000000 (ovvero il numero 64 in decimale) rappresentano un quartetto costituito da una cella con una pedina del giocatore 1 e 3 celle vuote. Per isolare tutti i gruppi di 8 bit all'interno di ogni \textbf{int} bastano solo 2 shift.

Vediamo un caso specifico per cercare di spiegare meglio l'algoritmo, e supponiamo ad esempio di voler analizzare il quartetto di celle verticale indicato in appendice C; le operazioni che abbiamo deciso di fare sono le seguenti:
\begin{enumerate}
	\item individuazione dell'\textbf{int} all'interno del quale si trova il quartetto da analizzare;
	\item individuazione del quartetto da analizzare all'interno dell'\textbf{int};
	\item shift a sinistra per portare gli 8 bit del quartetto negli 8 bit più significativi dell'\textbf{int};
	\item shift a destra per portare gli 8 bit del quartetto negli 8 bit meno significativi dell'\textbf{int}.
\end{enumerate}

A questo punto, quello che abbiamo ottenuto è un numero decimale compreso fra 0 e 255 che individua univocamente il tipo di quartetto; nel caso specifico, indicando con "X" le pedine del giocatore 1 e con "O" quelle del giocatore 2, abbiamo trovato il quartetto "X \_ X O", che corrisponde al numero decimale 70.

Ripetendo questa operazione per tutte le sequenze di 8 bit nelle 4 possibili direzioni si ottengono tutte le features.

Di seguito elenchiamo (senza entrare nei dettagli) alcune delle complicazioni che abbiamo incontrato:
\begin{itemize}
	\item ricordando che \textit{calcola\_features} prende in input una regione della scacchiera (che in generale non coincide con tutta la scacchiera), i primi e gli ultimi bit per ogni riga, colonna o diagonale, devono essere trattati separatamente;
	\item oltre a tutti i possibili gruppi di 8 bit all'interno di ogni \textbf{int}, bisogna considerare anche i gruppi di 8 bit che si trovano fra un \textbf{int} e il successivo (avendo cura di non uscire dai limiti della regione della scacchiera in cui si sta effettuando il calcolo);
	\item leggendo la scacchiera in diagonale, il numero di celle contenuto all'interno di ogni diagonale cambia in base alla diagonale, quindi abbiamo dovuto dividere la scacchiera in 3 regioni e trovare i limiti all'interno dei quali muoverci (vedere il codice per ulteriori dettagli).
\end{itemize}


\subsubsection{Schema di funzionamento di allena}
La funzione allena di \emph{stato\_partita} è la fase dell'allenamento più a basso livello; lo schema di funzionamento è molto semplice:
\begin{enumerate}
	\item genera 2 n-uple casuali di coefficienti per l'euristica;
	\item il computer gioca contro sé stesso: giocatore 1 utilizza la prima n-upla di coefficienti per il calcolo dell'euristica, mentre giocatore 2 usa la seconda;
	\item viene decretato un vincitore dopo un numero prestabilito di partite (ad esempio ci siamo resi conto che 100 è un buon compromesso: un numero inferiore darebbe risultati troppo aleatori, mentre un numero superiore è inutilmente eccessivo);
	\item l'n-upla del giocatore perdente evidentemente dà risultati peggiori nel calcolo dell'euristica, quindi viene sostituita con un'altra n-upla generata casualmente;
	\item si riparte dal punto 2 e si ripete il ciclo per un numero prestabilito di volte.
\end{enumerate}

Bisogna notare che il metodo \emph{minimax} per il calcolo della mossa successiva è deterministico, una volta fissati i coefficienti per il calcolo dell'euristica. Ciò significa che, facendo scontrare due giocatori con 2 n-uple di coefficienti diverse, questi faranno sempre le stesse mosse, e tutte le partite si concluderanno allo stesso modo; per ovviare a questo problema (che è un limite nel decretare quale delle 2 n-uple di coefficienti è effettivamente migliore), abbiamo deciso di rendere la prima mossa di ogni giocatore casuale. Questo non inficerà praticamente in alcun modo il resto della partita (nel gioco \emph{forza 4} la prima mossa è sostanzialmente ininfluente), ma garantirà il fatto che ogni partita è diversa dall'altra, anche quando si scontra per più volte la stessa coppia di \emph{n-uple}.

\subsubsection{Altre funzioni}
Le altre funzioni della classe che non abbiamo descritto un po' più in dettaglio sono semplici applicazioni o modifiche delle funzioni che abbiamo già visto:
\begin{itemize}
	\item \textbf{rimuovi\_pedina}: l'algoritmo è lo stesso di inserisci\_pedina;
	\item \textbf{euristica\_globale} e \textbf{aggiorna\_euristica}: prima calcolano le features tramite l'apposita funzione, e poi danno il risultato dell'euristica facendo una combinazione lineare con i coefficienti dati in input;
	\item \textbf{check\_vittoria}: chiama la funzione calcola\_features e controlla se ci sono quartetti vincenti; inoltre, verifica anche se la scacchiera è piena (in tal caso, la partita è finita in parità). 
\end{itemize}


\subsection{Forza4}
Questa classe gestisce le funzioni più ad alto livello del gioco, che principalmente sono le seguenti:
\begin{itemize}
	\item \textbf{gioca\_vs\_umano}();
	\item \textbf{gioca\_vs\_pc}();
	\item \textbf{allena}(numero\_cicli).
\end{itemize}

Le prime due sono semplici applicazioni di quasi tutte le funzioni di \emph{stato\_partita}.

Per quanto riguarda la terza, è un'applicazione più ad alto livello dell'analoga funzione di \emph{stato\_partita}; per la precisione, chiama quest'ultima un numero prefissato di volte (quello indicato in input) e alla fine di ogni ciclo salva su file l'n-upla migliore ottenuta fino a quel momento.

\section{Risultati sperimentali}
Una volta implementata la nostra soluzione, ne abbiamo verificato la bontà. Ovvero abbiamo verificato che l'algoritmo fosse in grado di vincere la maggior parte delle partite.

\subsection{Impostazione dell'esperimento}
Abbiamo chiesto a un campione di 10 persone di giocare contro il computer. Ognuna di esse ha giocato 2 partite su una scacchiera standard (6x7), una con difficoltà\footnote{La difficoltà coincide con la profondità a cui scende minimax.} 4, una con difficoltà 6. Tutte le partite sono state giocate dopo aver fatto allenare il computer con 100 cicli da 100 $n$-uple con profondità 1.\footnote{La motivazione per cui è stata scelta tale profondità è spiegata in \ref{allenamentosection}}

\subsection{Dati sperimentali}
I dati che abbiamo raccolto sono i seguenti (V = vinta dal giocatore umano, P = persa dal giocatore umano, p = pari):
\begin{table}[H]
	\centering
	\begin{tabular}{|l|r|r|}
		\hline
		\textbf{Giocatore} 	& \textbf{Difficoltà 4} 	& \textbf{Difficoltà 6} \\
		\hline
		1			& V				& P \\
		2			& P				& P \\
		3			& P				& P \\
		4			& V				& V \\
		5			& P				& P \\
		6			& P				& V \\
		7			& P				& P \\
		8			& P				& V \\
		9			& V				& p \\
		10			& P				& V \\
		\hline
		\hline
		\textbf{Vittorie:}	& $30\%$		& $40\%$ \\
		\hline
	\end{tabular}
\end{table}

\subsection{Analisi dati}
Data la limitatezza del campione preso in esame, vediamo qual è la probabilità di ottenere una percentuale di vittorie inferiore al $50\%$. Per fare ciò, calcoliamo la probabilità ($P$) di ottenere un dato numero di vittorie sperimentali ($n_{\text{sperim}}$) su $N$ partite in funzione della probabilità reale di vittoria del computer ($P_{\text{reale}}$). Tale probabilità segue questa legge:\footnote{Fissata una probabilità reale, si può calcolare la probabilità di ottenere determinati dati sperimentali utilizzando la distribuzione binomiale. Calcolando questo valore per ogni probabilità reale, si ottiene questa legge.}

\[P = \binom{N}{n_{\text{sper}}} {(P_{\text{reale}})}^{n_{\text{sper}}}(1-P_{\text{reale}})^{N-{n_{\text{sper}}}}\]

\begin{figure}
	\centering
	\includegraphics[clip=true,width=0.8\linewidth]{graficoprob1.pdf}
	\caption{$P$ nel caso difficoltà 4}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[clip=true,width=0.8\linewidth]{graficoprob2.pdf}
	\caption{$P$ nel caso difficoltà 6}
\end{figure}

La probabilità massima si ha proprio sul valore che otteniamo sperimentalmente, mentre misuriamo la deviazione utilizzando la larghezza della curva all'altezza di $0.15$, ovvero la larghezza della regione attorno al massimo in cui la probabilità è superiore a tale valore. Nel nostro caso, quindi, otteniamo:

\begin{table}[H]
	\centering
	\begin{tabular}{|l|r|r|}
		\hline
						& $P_{\text{max}}$ & Semideviazione \\
		\hline
		Difficoltà 4 	& $0.7$ & $0.155$ \\
		Difficoltà 6	& $0.6$ & $0.155$ \\
		\hline
	\end{tabular}
\end{table}

Pertanto, seppur il campione fosse limitato, possiamo affermare con buona affidabilità che la probabilità di vittoria da parte del computer sia superiore al $50\%$ nel caso di giocatori non esperti, ma comunque discretamente bravi.

\section{Conclusioni e possibili miglioramenti}
Il progetto ha raggiunto gli obiettivi preposti: il giocatore automatico risponde in maniera efficace e in tempo reale al giocatore umano, mentre l'allenamento è abbastanza efficiente da poter essere completato in poche ore.

È comunque possibile migliorare ancora l'algoritmo sia in efficienza che in allenamento. Ad esempio introducendo l'alpha–beta pruning si può ridurre il numero di rami dell'albero delle mosse che l'algoritmo minimax esplora. Si può migliorare l'algoritmo di verifica della vittoria, che è utile, più che per la partita in gioco, per l'algoritmo minimax stesso. Si può migliorare, ancora, quello di allenamento, sfruttando le vecchie $n$-uple per generarne di nuove anziché farlo in maniera totalmente casuale. Si possono aggiungere nuove features all'euristica per renderla più precisa.

\clearpage

\begin{appendices}
\section*{\textbf{Appendice A}\\Schema di stato\_partita}
\includepdf[scale=1]{immagini/stato_partita}

\section*{\textbf{Appendice B}\\Schema di funzionamento di inserisci\_pedina}
\includepdf[scale=1]{immagini/inserisci_pedina}

\section*{\textbf{Appendice C}\\Schema di funzionamento di calcola\_features}
\includepdf[scale=1]{immagini/calcola_features}

\end{appendices}

\end{document}