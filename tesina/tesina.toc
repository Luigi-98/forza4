\babel@toc {italian}{}
\babel@toc {italian}{}
\babel@toc {english}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {I}Introduzione}{2}% 
\contentsline {section}{\numberline {II}La nostra soluzione}{2}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {II-A}}Giocare una partita}{2}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {II-B}}Scelta automatica della mossa}{3}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {II-C}}Minimax}{3}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {II-D}}Funzione di euristica}{4}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {II-E}}Allenamento}{5}% 
\contentsline {section}{\numberline {III}Implementazione}{6}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {III-A}}Interfaccia}{6}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {III-B}}Minimax}{6}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {III-C}}Stato partita}{6}% 
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {III-C}1}Schema di funzionamento di inserisci\_perdina}{7}% 
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {III-C}2}Schema di funzionamento di calcola\_features}{8}% 
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {III-C}3}Schema di funzionamento di allena}{9}% 
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {III-C}4}Altre funzioni}{9}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {III-D}}Forza4}{13}% 
\contentsline {section}{\numberline {IV}Risultati sperimentali}{13}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {IV-A}}Impostazione dell'esperimento}{13}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {IV-B}}Dati sperimentali}{13}% 
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {IV-C}}Analisi dati}{13}% 
\contentsline {section}{\numberline {V}Conclusioni e possibili miglioramenti}{14}% 
